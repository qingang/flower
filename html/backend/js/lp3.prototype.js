Array.prototype.unique = function () {
	var self = this,
		_a = self.concat().sort();

	_a.sort(function (a, b) {
		if (a === b) {
			var n = self.indexOf(b);
			self.splice(n, 1);
		}

		return -1;
	});
	return _a;
};

Array.prototype.contains = function (elem) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == elem) return true;
	}
	return false;
};

Array.prototype.unset = function (value) {
	var position = $.inArray(value, this);
	if (position != -1) this.splice(position, 1);
};

Array.prototype.toString = function () {
	var _str = '';
	for (var i = 0; i < this.length; i++) {
		if (_str != '') {
			_str += ',';
		}
		_str += this[i];
	}
	return _str;
};

String.prototype.startsWith = function (str) {
	return (this.match("^" + str) == str);
};

String.prototype.endsWith = function (str) {
	return (this.match(str + "$") == str);
};

String.prototype.dateFormat = function (pattern) {
	return (new Date(Date.parse(this.replace(/-/g, "/")))).pattern(pattern);
};
String.prototype.formatDate = function (year, month, date) {
	month = month + 1;
	if (month < 10) month = '0' + month;
	if (date < 10) date = '0' + date;
	return year + '-' + month + '-' + date;
};

String.prototype.trim = function () {
	return (this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""));
};

String.prototype.ChineseLength = function () {
	return this.replace(/[\u4E00-\u9FA5]/g, "***").length;
};

String.prototype.replaceAll = function (s1, s2) {
	return this.replace(/s1/g, s2);
};

String.prototype.formatMoney = function () {
	return this.replace(/\B(?=(?:[0-9]{3})+$)/g, ',');
};

String.prototype.isMoney = function () {
	return /^([+-]?)((\d{1,3}(,\d{3})*)|(\d+))(\.\d{2})?$/.test(this)
};

String.prototype.dotString = function (len) {
	var l = this.length;
	var ll = Math.min(len, l);
	if (len > l - 3 && len < l)
		ll = l - 3;
	else if (len < l - 3)
		ll -= 3;
	var s = '';
	for (var i = 0; i < ll; i++) {
		s += this.charAt(i);
	}
	if (len < l)
		s += '...';
	return s;
};
String.prototype.toHHmm = function () {
	var minute = parseInt(this);
	if (!isNaN(minute)) {
		var h = Math.floor(minute / 60);
		var m = minute % 60;
		if (h > 0) {
			if (m > 0) {
				return h + i18n.get('hour') + m + i18n.get('minute');
			} else {
				return h + i18n.get('hour');
			}
		} else {
			if (m > 0) {
				return m + i18n.get('minute');
			} else {
				return "0" + i18n.get('minute');
			}
		}
	} else {
		return this;
	}
};
Date.prototype.pattern = function (fmt) {
	var o = {
		"M+": this.getMonth() + 1, //月份
		"d+": this.getDate(), //日
		"h+": this.getHours() % 12 == 0 ? 12 : this.getHours() % 12, //小时
		"H+": this.getHours(), //小时
		"m+": this.getMinutes(), //分
		"s+": this.getSeconds(), //秒
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度
		"S": this.getMilliseconds() //毫秒
	};
	var week = {
		"0": "\u65e5",
		"1": "\u4e00",
		"2": "\u4e8c",
		"3": "\u4e09",
		"4": "\u56db",
		"5": "\u4e94",
		"6": "\u516d"
	};
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	if (/(E+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\u661f\u671f" : "\u5468") : "") + week[this.getDay() + ""]);
	}
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
};

Date.prototype.isLeapYear = function () {
	var year = this.getFullYear();
	return (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0);
};

Date.prototype.getMonthDays = function () {
	var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	var month = this.getMonth();
	var days = monthDays[month];
	return (this.isLeapYear() && month == 1) ? 29 : days;
};

Date.prototype.getMonthBeginDate = function () {
	return 1;
};

Date.prototype.getMonthEndDate = function () {
	var nowDate = this.getDate();
	var endDate = this.getMonthDays();
	return this.dayDiff(endDate - nowDate);
};

Date.prototype.dayDiff = function (days) {
	return new Date(this.getTime() + days * 24 * 60 * 60 * 1000);
};
Date.prototype.dateToStrShort = function () {
	var month = this.getMonth() + 1;
	var year = this.getFullYear();
	var date = this.getDate();
	if (month < 10) month = '0' + month;
	if (date < 10) date = '0' + date;
	return year + '-' + month + '-' + date;
};

jQuery.fn.outer = function () {
	return $($('<div></div>').html(this.clone())).html();
};

String.prototype.camelize = function () {
	var parts = this.split('-'), len = parts.length;
	if (len == 1) return parts[0];

	var camelized = this.charAt(0) == '-'
		? parts[0].charAt(0).toUpperCase() + parts[0].substring(1)
		: parts[0];

	for (var i = 1; i < len; i++)
		camelized += parts[i].charAt(0).toUpperCase() + parts[i].substring(1);

	return camelized;
};

String.prototype.capitalize = function () {
	return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
};

String.prototype.isInteger = function () {
	//var patrn=/^[0-9]*[1-9][0-9]*$/;   //正整数
	var patrn = /^\d+$/; //正整数+0
	return patrn.test(this);
};

Number.prototype.toWeek = function () {
	if (this == 0) {
		return i18n.get('sunday');
	} else if (this == 1) {
		return i18n.get('monday');
	} else if (this == 2) {
		return i18n.get('tuesday');
	} else if (this == 3) {
		return i18n.get('wednesday');
	} else if (this == 4) {
		return i18n.get('thursday');
	} else if (this == 5) {
		return i18n.get('friday');
	} else if (this == 6) {
		return i18n.get('saturday');
	} else {
		return '';
	}
}