/* ==========================================================
 * AdminKIT v1.3
 * common.js
 * 
 * http://www.mosaicpro.biz
 * Copyright MosaicPro
 *
 * Built exclusively for sale @Envato Marketplaces
 * ========================================================== */ 

/* Utility functions */





$(function()
{
	
	/* Slim Scroll Widgets */
	$('.widget-scroll').each(function(){
		$(this).find('.slim-scroll').slimScroll({
			height: $(this).attr('data-scroll-height')
	    });
	});
	
	/* Other non-widget Slim Scroll areas */
	$('.wrap-container .slim-scroll').each(function(){
		$(this).slimScroll({
			height: $(this).attr('data-scroll-height'),
			allowPageScroll : false,
			railDraggable: ($.fn.draggable ? true : false)
	    });
	});

});