var treeData = [
	{
		"id" : 1,
		"pid" : 0,
		"name" : "项目库",
		"open" : true,
		"isParent" : true,
		"children" : [
			{
				"id" : 4,
				"pid" : 1,
				"name" : "基础题",
				"isParent" : true,
				"children" : []
			},
			{
				"id" : 5,
				"pid" : 1,
				"name" : "护理题",
				"isParent" : true,
				"children" : []
			}
		]
	},
	{
		"id" : 2,
		"pid" : 0,
		"name" : "外科共享库",
		"isParent" : true,
		"open" : true,
		"children" : [
			{
				"id" : 6,
				"pid" : 2,
				"name" : "基础题",
				"isParent" : true,
				"children" : []
			},
			{
				"id" : 7,
				"pid" : 2,
				"name" : "护理题",
				"isParent" : true,
				"children" : []
			}
		]
	},
	{
		"id" : 3,
		"pid" : 0,
		"name" : "内科共享库",
		"open" : true,
		"isParent" : true,
		"children" : [
			{
				"id" : 8,
				"pid" : 3,
				"name" : "基础题",
				"isParent" : true,
				"children" : []
			},
			{
				"id" : 9,
				"pid" : 3,
				"name" : "护理题",
				"isParent" : true,
				"children" : []
			}
		]
	}
]