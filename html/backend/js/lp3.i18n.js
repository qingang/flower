LP3.i18n = {

	get: function () {
		var a = arguments;
		if (a.length == 1) {
			return (typeof i18n[a[0]] != 'undefined') ? i18n[a[0]] : a[0];
		} else {
			var _v = (typeof i18n[a[0]] != 'undefined') ? i18n[a[0]] : a[0];
			var _s;
			for (var i = 1; i < a.length; i++) {
				_s = '\\{' + (i - 1) + '\\}';
				_v = _v.replace(new RegExp(_s, 'g'), a[i]);
			}
			return _v;
		}
	},

	no_record_selected: '没有任何记录被选中！',
	data_is_null: '数据为空或格式错误!',
	request_fail: '查询失败，请稍候再试或咨询系统管理员。',

	select_curr_page_records: '本页的{0}条记录',
	select_all_records: '所有查询结果的{0}条记录',

	no_searched: '未找到符合条件的数据！',
	require_pic: '上传的文件格式必须是jpg,jpeg,png,gif',

	pager_msg_loading: "正在加载数据，请稍等！...",
	page_msg_no_data: "没有找到数据！",
	page_msg_previous_page: "上页",
	page_msg_next_page: "下页",
	page_msg_first_page: '首页',
	page_msg_last_page: '尾页',
	page_msg_single_select: '单选',
	page_msg_multi_select: '复选',
	pager_msg_cur_total: ' 当前第{0}条到第{1}条，共{2}条',

	network_connection_fails: '网络连接失败',

	validation_message_required: '{0}必须填！',
	validation_message_email: '{0}必须输入正确格式的电子邮件！',
	validation_message_url: '{0}必须输入正确格式的网址！',
	validation_message_date: '{0}必须输入正确格式的日期！',
	validation_message_number: '{0}必须输入合法的数字(负数，小数)！',
	validation_message_digits: '{0}必须输入整数！',
	validation_message_maxlength: '{0}输入长度最多是{1}的字符串(汉字算三个字符)！',
	validation_message_minlength: '{0}输入长度最小是{1}的字符串(汉字算三个字符)！',
	validation_message_rangelength: '{0}输入长度必须介于 {1} 和 {2} 之间的字符串(汉字算三个字符)！',
	validation_message_range: '{0}输入值必须介于 {1} 和 {2} 之间！',
	validation_message_max: '{0}输入值不能大于{1}！',
	validation_message_min: '{0}输入值不能小于{1}！',
	validation_duplicate: '{0}已经存在!',
	validation_message_money: '{0}必须输入正确格式的价格！',
	validation_message_phone: '{0}必须输入正确格式的电话号码！',

	submit_success: '提交成功',
	not_a_number: '不是数字',
	not_a_positive_number: '不是正数,请输入正数',
	not_a_positive_integer: '不是正整数',
	hour : '小时',
	minute : '分钟',
	monday : '星期一',
	tuesday : '星期二',
	wednesday : '星期三',
	thursday : '星期四',
	friday : '星期五',
	saturday : '星期六',
	sunday : '星期日'
};

if (typeof(i18n) == 'undefined') {
	i18n = LP3.i18n;
}