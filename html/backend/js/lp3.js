var LearningPlatformV3 = {
	name: 'huihe_jsbase',
	description: 'Javascrip Base Library for Learning Platform Version 3',
	version: "3.0.0",

	getParameter: function (name) {
		return decodeURI(
			(RegExp(name + '=' + '(.+?)(&|$)').exec(location.href) || [, null])[1]
		)
	},

	extend: function (destination, source) {
		for (var property in source)
			destination[property] = source[property];
		return destination;
	},

	loadJs: function () {
		$.each(arguments, function (idx, v) {
			$('<script type="text/javascript" src="' + v + '"></script>').appendTo(document.body);
		})
	},

	loadCss: function () {
		$.each(arguments, function (idx, v) {
			$('<link href="' + v + '" rel="stylesheet" type="text/css" />').appendTo(document.body);
		})
	},

	debugMode: true,

	init: function () {
		if (window.location.host.startsWith("localhost") || window.location.host.match(/[\d]+\.[\d]+\.[\d]+\.[\d]+\./)) this.debugMode = true;
		this.createLogger();
		if (top.window == window) {
			LP3.MSG.init();
			LP3.Loading.init();
		}

		$(window).bind('unload', function () {
			LP3.MSG.hide();
		});

		$(document.body).bind(
			"ajaxSend",
			function () {
				LP3.Loading.show();
			}).bind("ajaxComplete", function () {
				LP3.Loading.hide();
			});

		$(':input[type="text"]').each(function () {
			$(this).attr('autocomplete', 'off')
		});
		
		$('[rel=tooltip]').tooltip({
			position: {
				my: "center bottom-10",
				at: "center top",
				using: function( position, feedback ) {
					$( this ).css( position );
					$( "<div>" ).addClass( "arrow" ).appendTo( this );
				}
			}
		});
	},

	createLogger: function () {
		if (top.window == window) {
			if (this.debugMode) {
				$('<div id="huihe-logger-window" style="display:none;"><div width="100%"><span style="width:640px; float:left;">&nbsp;LP3 Logger::Powered by Log4Javascript</span><span id="loggerSwitcher" style="cursor:pointer">SHOW</span></div><div id="huihe-logger"></div></div>').appendTo('body');
				window.log = log4javascript.getLogger("main");
				var appender = new log4javascript.InPageAppender();
				log.addAppender(appender);
				$("#huihe-logger-window").draggable();
				$("#loggerSwitcher").bind('click', function () {
					var ls = $("#loggerSwitcher");
					if (ls.html() == 'HIDE') {
						ls.html('SHOW');
						$('#huihe-logger').hide();
					} else {
						ls.html('HIDE');
						$('#huihe-logger').show();
					}
				});
			} else {
				window.log = {
					trace: function () {
					},
					error: function () {
					},
					debug: function () {
					},
					info: function () {
					},
					warn: function () {
					}
				}
			}
		} else {
			window.log = parent.log;
		}
	},

	goToURL: function (url) {
		location.assign(url);
	},

	refreshCurrentPage: function () {
		window.location.reload();
	},

	setCookie: function (name, value) {
		document.cookie = name + '=' + escape(value);
	},

	getCookie: function (name) {
		var arr = document.cookie.match(new RegExp("(^| )" + name + "=([^;]*)(;|$)"));
		if (arr != null) return unescape(arr[2]);
		else return null;
	},

	deleteCookie: function (name) {
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = this.getCookie(name);
		if (cval != null) document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
	},

	checkScreen: function () {
		try {
			var width = screen.width;
			var height = screen.height;
			var availWidth = $(window).width();
			var availHeight = $(window).height();
			if (LP3.getCookie('noticeScreen') == null) {
				if (availWidth < 1000 || availHeight < 500) {
					LP3.MSG.warn('你所使用的浏览器窗口太小，将会影响页面正常显示，请最大化当前窗口或使用F11全屏使用系统！', true);
					LP3.setCookie('noticeScreen', true);
				} else {
					if (width < 1280 || height < 720) {
						LP3.MSG.warn('你所使用的屏幕分辨率过小，将会影响页面正常显示！建议使用1280x720以上的分辨率。', true);
						LP3.setCookie('noticeScreen', true);
					}
				}
			}
		} catch (e) {
			log.error(e);
		}
	}
};

var LP3 = LearningPlatformV3;

LP3.Loading = (top.window == window) ? {
	defaultOptions: {

	},
	options: {
		enabled: true
	},

	init: function () {
		log.error('initializing loading with ' + window.location.href);
		$('<div id="huihe-global-loading"><img height="20" src="css/images/loading.gif" class="middle">&nbsp;Loading...</div>').appendTo('body');
	},

	show: function () {
		if (this.options.enabled) {
			$('#huihe-global-loading').show();
		}
	},

	hide: function () {
		$('#huihe-global-loading').hide();
	},

	disable: function () {
		this.options.enabled = false;
	},

	enable: function () {
		this.options.enabled = true;
	}
} : parent.LP3.Loading;

var Loading = LP3.Loading;

LP3.MSG = (top.window == window) ? {
	callWindow: 'top',
	container: null,
	html: '',
	autoHide: true,

	timerHandle: null,

	defaultOptions: {
		interval: 6 * 1000,

		showIcon: true,
		showClose: false,
		divClass: '',

		background: '',
		color: '',
		borderColor: '',
		width: ''
	},

	options: {},

	setCallWindow: function (win) {
		this.callWindow = win;
	},

	init: function () {
		log.debug('initializing msg...');
		var self = this;
		$('<div id="huihe-global-msg"></div>').appendTo('body');
		self.container = $('#huihe-global-msg');
		self.hide();
	},

	_show: function (type, args) {
		if (null != this.timerHandle) clearTimeout(this.timerHandle);
		log.debug('show msg:' + args[0]);

		var self = this;

		var a = args;
		var autoHide = a.length > 1 ? autoHide = a[1] : self.autoHide;

		if (a.length > 2) {
			self.options = LP3.extend(self.defaultOptions, args[2]);
		} else {
			self.options = self.defaultOptions;
		}

		$('#huihe-global-msg').removeClass('msg_success').removeClass('msg_error').removeClass('msg_warn').removeClass('msg_info').addClass('msg_' + type).html('<img src="css/images/'+type+'.png"/>&nbsp;&nbsp;'+args[0]);

		this.show();

		if (autoHide) {
			this.timerHandle = window.setTimeout(function () {
				self.hide();
			}, self.defaultOptions.interval);
		}
	},

	empty: function () {
		$('#huihe-global-msg').empty();
	},

	show: function () {
		this.container.show('slow');
	},

	hide: function () {
		this.container.hide();
		this.empty();
	},

	info: function () {
		this._show('info', arguments);
	},

	warn: function () {
		this._show('warn', arguments);
	},

	success: function () {
		this._show('success', arguments);
	},

	error: function () {
		this._show('error', arguments);
	}

} : parent.LP3.MSG;

var MSG = LP3.MSG;

if(!$.browser) {
	// Useragent RegExp
	var rwebkit = /(webkit)[ \/]([\w.]+)/,
		ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/,
		rmsie = /(msie) ([\w.]+)/,
		rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/;

	var ua = navigator.userAgent.toLowerCase();
	var match = rwebkit.exec( ua ) ||
		ropera.exec( ua ) ||
		rmsie.exec( ua ) ||
		ua.indexOf("compatible") < 0 && rmozilla.exec( ua ) ||
		[];

	$.browser = {};
	$.browser.webkit = false;
	$.browser.mozilla = false;
	$.browser.opera = false;
	browser = match[1] || "";
	$.browser[browser] = true;
	$.browser.version = match[2] || "0";
}

$(document).ready(function () {
	LP3.init();
});