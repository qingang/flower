LP3.Pager = function (id, options) {
	this.id = '';//分页组件显示在哪个地方 dom element id
	this.data = {
		curPage: 1,//当前是第几页，从1开始
		perPage: 20,//每页显示多少条记录，默认为20，如果<0表示不分页
		totalRecords: 0,//总共有多少条符合条件的记录
		totalPages: 0,//总共有多少页
		pageData: [],//当前页的数据
		error: false,
		displayRecords: 0
	};
	this.options = {
		useClientCache: false,//是否使用客户端缓存，目前还没有调试好，不建议使用
		loadDataURL: '',//分页组件获取数据的URL,还会有其他的参数由分页组件动态算出
		loadDataCallback: null,//加载数据后的回调函数，回调时会给出加载的数据
		showNav: true,//是否显示下面的导航
		showPager: true,//是否显示分页组件,不显示的时候分页组件可以作为客户端的一个后台查询工具
		columns: [],//显示的列信息
		showColumns: 0,//这个是计算出来的，表明总有会显示多少个列
		parameters: {},//分页查询的固定参数
		sorts: {},//分页查询的固定排序字段
		action: false,//全局的操作，目前支持全选和单选
		defaultPerPageArray: [10, 20, 30, 50, 100, 200, 500],//下面导航中可以选择的每页记录数
		loadOnInit: true,//是否在初始化的时候就加载第一页数据
		oldParameterMode: false,//兼容老的参数传递方式
		global: true,
		noResultMsg: null,//没有加载到数据时候显示的信息
		callbackBeforeRender: true,//加载数据回调函数调用的时机，true表示在显示加载的数据之前调用，false表示在显示加载的数据之后调用
		filter: function (rowData) {
			return true;
		},
		saveSelectedRecords: false,
		pkField: 'id',
		savedSelectedRecords: {},
		showColumnsSelector: false
	};
	this.conditions = {};
	this.sorts = {};
	this.cache = {};
	this.renders = { //默认实现的列渲染器
		sequence: function (rowData, rowIndex, field) {
			if (rowData['perPage'] < 1) return (rowIndex + 1);
			else return ((rowData['curPage'] - 1) * rowData['perPage'] + rowIndex + 1);
		},//显示数据记录序号的列渲染器
		returnSelf: function (rowData, rowIndex, field) {
			var v = rowData[field];
			if (v == null || v == undefined) v = '';
			return v;
		},//对列数据不做任何处理的列渲染器
		date: function (rowData, rowIndex, field) {
			var v = rowData[field];
			if (v == undefined || v == 'null' || v == 'NULL' || v == null) return '';
			if (/^\d+$/.test(v)) {
				var date = new Date();
				date.setTime(v);
				return date.pattern('yyyy-MM-dd');
			} else {
				return v;
			}
		},//显示日期的列渲染器，可以把日期格式化为yyyy-MM-dd格式
		list: function (rowData, rowIndex, field) {
			var v = rowData[field];
			var result = "";
			$.each(v, function (index, value) {
				if (value == undefined || value == 'null' || value == 'NULL' || value == null) {
					value = '';
				}
				result = result + value + '<br>';
			});
			return result;
		},//对列表数据进行渲染的列渲染器
		dateTime: function (rowData, rowIndex, field) {
			var v = rowData[field];
			if (v == undefined || v == 'null' || v == 'NULL' || v == null) return '';
			if (/^\d+$/.test(v)) {
				var date = new Date();
				date.setTime(v);
				return date.pattern('yyyy-MM-dd HH:mm:ss');
			} else {
				return v;
			}
		},//显示日期时间的列渲染器，可以把日期时间格式化为yyyy-MM-dd HH:mm:ss格式
		time: function (rowData, rowIndex, field) {
			var v = rowData[field];
			if (v == undefined || v == 'null' || v == 'NULL' || v == null) return '';
			if (/^\d+$/.test(v)) {
				var date = new Date();
				date.setTime(v);
				return date.pattern('HH:mm:ss');
			} else {
				return v;
			}
		}//显示时间的列渲染器，可以把时间格式化为HH:mm:ss格式
	};
	this.actions = {
		multi: 'checkbox',
		single: 'radio'
	};
	this.currentSort = null;
	this.init(id, options);
};

LP3.Pager.prototype = {
	init: function (id, options) {
		this.id = id;
		options = options || {};
		if (options['curPage'] != undefined) {
			this.data.curPage = options.curPage;
			delete options.curPage;
		}
		if (options['perPage'] != undefined) {
			this.data.perPage = options.perPage;
			delete options.perPage;
		}
		LP3.extend(this.options, options);
		log.debug('pager, global:' + this.options.global);
		this.initColumn();
		if (this.options.loadOnInit) this.loadData();
	},
	initColumn: function () {
		for (var i = 0; i < this.options.columns.length; i++) {
			var defaultColumnOptions = {show: true, sortable: false, render: this.renders['returnSelf']};
			LP3.extend(defaultColumnOptions, this.options.columns[i]);
			var field = defaultColumnOptions['field'];
			var title = (defaultColumnOptions['title']);
			if (title == undefined) {
				title = i18n[field];
				if (title == undefined) {
					title = field;
					if (title == undefined) title = '';
					else title = title.capitalize();
				}
			}
			defaultColumnOptions['title'] = title;
			if (defaultColumnOptions['show']) this.options.showColumns++;
			this.options.columns[i] = defaultColumnOptions;
		}
		if (this.options.action) this.options.showColumns++;
	},
	getColumnSortClass: function (column) {
		if (column['sortable']) {
			var field = column['field'];
			return this.getSortableFieldSortClass(field);
		}
		return '';
	},
	getSortableFieldSortClass: function (field) {
		var sortOrder;
		if (this.currentSort != null && this.currentSort.length > 0) {
			for (var i = 0; i < this.currentSort.length; i++) {
				if (this.currentSort[i]['field'] == field) {
					sortOrder = this.currentSort[i]['order'];
					return 'sortable currentSort ' + ('none' == sortOrder ? '' : 'sort' + sortOrder);
				}
			}
		}
		if (this.sorts[field] != undefined) {
			sortOrder = this.sorts[field];
			return 'sortable ' + ('none' == sortOrder ? '' : 'sort' + sortOrder);
		}
		return 'sortable';
	},
	getColumnSortIndex: function (column) {
		if (column['sortable']) {
			return this.getSortableFieldSortIndex(column['field']);
		}
		return 0;
	},
	getSortableFieldSortIndex: function (field) {
		if (this.currentSort != null) {
			if (this.currentSort.length < 2) return 0;
			for (var i = 0; i < this.currentSort.length; i++) {
				if (this.currentSort[i]['field'] == field) {
					return (i + 1);
				}
			}
		}
		return 0;
	},
	showPager: function (init) {
		this.data.displayRecords = 0;
		if (this.options.showPager) {
			if (null == this.data || this.data.pageData.length < 1) {
				html = '<table style="width:100%;text-align:center;" class="content" align="center"><tr height="135" style="text-align:center;" align="center"><td style="text-align:center;" align="center" width="100%"><img src="/images/letter.png" width="46" height="45">' + (this.options.noResultMsg == null ? i18n.no_searched : this.options.noResultMsg) + '</td></tr></table>';
				$('#' + this.id).empty();
				$(html).appendTo($('#' + this.id));
			} else {
				var html = '';
				if (this.options.showColumnsSelector) html += this.showColumnsSelector();
				html += '<table class="table mouseover" width="100%"><tr>';
				var column, i;
				if (this.options.action == 'multi') {
					html += '<th><input type="checkbox" pagerFlag="true" value="checkAll"></th>';
				} else if (this.options.action == 'single') {
					this.options.action = 'single';
					html += '<th>' + i18n.page_msg_single_select + '</th>'
				}
				for (i = 0; i < this.options.columns.length; i++) {
					column = this.options.columns[i];
					if (column['show']) {
						html += '<th field="' + column['field'] + '"';
						if (column['width'] != undefined) {
							html += ' style="width:' + column['width'] + 'px;"';
						}
						if (column['sortable']) {
							html += ' class="' + this.getColumnSortClass(column) + '"';
						}
						html += '>' + column['title'];
						if (column['sortable']) {
							var sortIndex = this.getColumnSortIndex(column);
							if (sortIndex > 0) {
								html += '<abbr style="margin-left:20px;background-color:#FFFFFF;">' + sortIndex + '</abbr>';
							}
						}
						html += '</th>';
					}
				}
				html += '</tr>';
				if (null == this.data || this.data.pageData.length < 1) {
					html += '<tr><td colspan="' + this.options.showColumns + '">';
					if (init) {
						if (this.options.loadOnInit) html += i18n.pager_msg_loading;
					} else {
						html += i18n.page_msg_no_data;
						if (this.data.error) html += this.data.error;
						this.data.error = false;
					}
					html += '</td></tr></table>';
				} else {
					for (var rowIndex = 0; rowIndex < this.data.pageData.length; rowIndex++) {
						var rowData = this.data.pageData[rowIndex];
						if (this.options.filter(rowData)) {
							this.data.displayRecords++;
							html += '<tr';
							if ($.browser.msie) {
								html += ' onMouseMove="javascript:this.bgColor=\'#CCCCCFF\';" onMouseOut="javascript:this.bgColor=\'#FFFFFF\';"';
							}
							html += '>';
							rowData['curPage'] = this.data['curPage'];
							rowData['perPage'] = this.data['perPage'];
							if (this.options.action) {
								html += '<td><input type="' + this.actions[this.options.action] + '" pagerFlag="true" value="' + rowData['id'] + '" rowIndex="' + rowIndex + '" name="' + this.id + '_pager_action"';
								if (this.options.saveSelectedRecords) {
									var pageDataField = 'page_' + this.data.curPage;
									var savePageData = this.options.savedSelectedRecords[pageDataField];
									if (savePageData != undefined) {
										for (var x = 0; x < savePageData.length; x++) {
											if (savePageData[x][this.options.pkField] == rowData[this.options.pkField]) html += ' checked="checked"';
										}
									}
								}
								html += '></td>';
							}
							for (var columnIndex = 0; columnIndex < this.options.columns.length; columnIndex++) {
								column = this.options.columns[columnIndex];
								if (column['show']) {
									var field = column['field'];
									var columnData = rowData[field];
									if (column['render'] != undefined) {
										if (typeof column['render'] == 'string') {
											column['render'] = this.renders[column['render']];
											if (column['render'] == undefined) column['render'] = this.renders['returnSelf'];
										}
										if (typeof column['render'] == 'function') {
											columnData = column['render'](rowData, rowIndex, field);
										}
									}
									if (column['template'] != undefined) {
										var data = LP3.extend({}, rowData);
										data[field] = columnData;
										data['rowIndex'] = rowIndex;
										var tplStr = '<script type="text/x-jquery-tmpl">' + column['template'] + '</script>';
										html += '<td style="white-space:nowrap;';
										if (column['textAlign'] != undefined) {
											html += "text-align:" + column['textAlign'] + ";";
										}
										html += '">' + $(tplStr).tmpl(data).outer() + '</td>';
									} else {
										html += '<td style="white-space:nowrap;';
										if (column['textAlign'] != undefined) {
											html += "text-align:" + column['textAlign'] + ";";
										}
										html += '">' + columnData + '</td>';
									}
								}
							}
							html += '</tr>';
						}
					}
					html += '</table>';
				}

				if (this.options.showNav) {
					html += '<div class="pager">';
					html += '<a href="javascript:void(0);" pageIndex="1">&lt;&lt; ' + i18n.page_msg_first_page + '</a>';
					if (this.hasPreviousPage()) {
						html += '<a href="javascript:void(0);" pageIndex="' + this.getPreviousPage() + '">&lt; ' + i18n.page_msg_previous_page + '</a>';
					}
					var navs = this.getPagerNavs();
					for (i = 0; i < navs.length; i++) {
						var pIndex = parseInt(navs[i]);
						if (isNaN(pIndex)) html += navs[i];
						else {
							html += '<a href="javascript:void(0);" pageIndex="' + pIndex + '"';
							if (pIndex == this.data.curPage) html += ' class="current"';
							html += '>' + pIndex + '</a>';
						}
					}
					if (this.hasNextPage()) {
						html += '<a href="javascript:void(0);" pageIndex="' + this.getNextPage() + '">' + i18n.page_msg_next_page + ' &gt;</a>';
					}
					html += '<a href="javascript:void(0);" pageIndex="' + (this.data.totalPages) + '">' + i18n.page_msg_last_page + ' &gt;&gt;</a>';
					html += i18n.get('pager_msg_cur_total', ((this.data.curPage - 1) * this.data.perPage + 1), (Math.min(this.data.totalRecords, this.data.curPage * this.data.perPage)), this.data.totalRecords);
					var perPage = this.data.perPage;
					var found = false;
					var perPageArray = [];
					for (var i = 0; i < this.options.defaultPerPageArray.length; i++) {
						if (this.options.defaultPerPageArray[i] == perPage) {
							found = true;
						}
						perPageArray[i] = this.options.defaultPerPageArray[i];
					}
					if (!found) {
						perPageArray.push(perPage);
					}
					html += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>每页记录数设定:<select id="' + this.id + '_perPageSelection">';
					for (var i = 0; i < perPageArray.length; i++) {
						html += '<option value="' + perPageArray[i] + '"';
						if (perPageArray[i] == perPage) {
							html += ' selected="selected"';
						}
						html += '>' + perPageArray[i] + '</option>';
					}
					html += '</select></span>';
					html += '</div>';
					html += '<div class="clear"></div>';
				}
				$('#' + this.id).empty();
				$(html).appendTo($('#' + this.id));
				if (this.options.action) this.bindActionEvent();
				this.bindSortEvent();
				if (this.options.showNav) this.bindNavEvent();
				if (this.options.showColumnsSelector) this.bindColumnSelectorEvent();
			}
		}
	},
	bindSortEvent: function () {
		var selector = '#' + this.id + ' th.sortable';
		var _self = this;
		$(selector).each(function () {
			$(this).bind('click', function () {
				_self.onChangeSort($(this).attr('field'));
			});
		});
	},
	onChangeSort: function (field) {
		/*
		 var selector = '#' + this.id + ' th.currentSort';
		 if ($(selector).attr('field') != field) {
		 $(selector).removeClass('currentSort');
		 $('#' + this.id + ' th[field=' + field + ']').addClass('currentSort');
		 }
		 if ($('#' + this.id + ' th[field=' + field + ']').attr('class').indexOf('sortDesc') != -1) {
		 $('#' + this.id + ' th[field=' + field + ']').removeClass('sortDesc');
		 $('#' + this.id + ' th[field=' + field + ']').addClass('sortAsc');
		 this.currentSort = {field : field, order : 'asc'};
		 } else {
		 $('#' + this.id + ' th[field=' + field + ']').removeClass('sortAsc');
		 $('#' + this.id + ' th[field=' + field + ']').addClass('sortDesc');
		 this.currentSort = {field : field, order : 'desc'};
		 }
		 this.setSortField(field, this.currentSort['order']);
		 */
		var sortOrder = this.sorts[field];
		if (sortOrder == 'Asc') sortOrder = 'Desc';
		else if (sortOrder == 'Desc') sortOrder = 'none';
		else sortOrder = 'Asc';
		this.sorts[field] = sortOrder;
		if (this.currentSort == null) this.currentSort = [];
		var found = false;
		for (var i = 0; i < this.currentSort.length; i++) {
			var sort = this.currentSort[i];
			if (sort['field'] == field) {
				found = true;
				if (sortOrder == 'none') {
					this.currentSort.splice(i, 1);
				} else {
					sort['order'] = sortOrder;
				}
				break;
			}
		}
		if (!found) {
			this.currentSort[this.currentSort.length] = {field: field, order: sortOrder};
		}
		var selector = '#' + this.id + ' th.sortable';
		var _self = this;
		$(selector).each(function () {
			var cf = $(this).attr('field');
			$(this).attr('class', _self.getSortableFieldSortClass(cf));
			var sortIndex = _self.getSortableFieldSortIndex(cf);
			if (sortIndex == 0) sortIndex = '';
			$(this).find('abbr').each(function () {
				$(this).text(sortIndex);
			});
		});
		this.refresh();
	},
	bindActionEvent: function () {
		var _self = this;
		if (this.options.action == 'multi') {
			$('#' + this.id + ' input:checkbox[pagerFlag=true]').bind('click', function () {
				if ($(this).attr('value') == 'checkAll') {
					_self.checkAll($(this).is(':checked'));
				} else {
					_self.fieldCheckChange($(this).is(':checked'), $(this).attr('rowIndex'), false);
				}
			});
		} else {
			$('#' + this.id + ' input:radio[pagerFlag=true]').bind('click', function () {
				_self.fieldCheckChange(true, $(this).attr('rowIndex'), false);
			});
		}
	},
	fieldCheckChange: function (flag, rowIndex, isAll) {
		var allChecked = true;
		if (!flag) {
			if (this.options.action == 'multi') {
				$('#' + this.id + ' th input:checkbox[pagerFlag=true]').attr('checked', flag);
				$('#' + this.id + ' th input:checkbox[pagerFlag=true]').attr('checked', flag);
				allChecked = false;
				/*
				 if(!isAll) {
				 var r = this.options.savedSelectedRecords['page_'+this.data['curPage']] || {};
				 var pk = this.data.pageData[rowIndex][this.options.pkField];
				 for(var i=0; i<r.length; i++) {
				 if(pk == r[i][this.options.pkField]) {
				 r.splice(i,1);
				 break;
				 }
				 }
				 this.options.savedSelectedRecords['page_'+this.data['curPage']] = r;
				 }
				 */
			}
		} else {
			if (this.options.action == 'multi') {
				$('#' + this.id + ' td input:checkbox[pagerFlag=true]').each(function () {
					if (!$(this).is(':checked')) allChecked = false;
				});
				$('#' + this.id + ' th input:checkbox[pagerFlag=true]').attr('checked', allChecked);
				/*
				 if(!isAll) {
				 var r = this.options.savedSelectedRecords['page_'+this.data['curPage']];
				 if(r == undefined) r = [];
				 r[r.length] = this.data.pageData[rowIndex];
				 this.options.savedSelectedRecords['page_'+this.data['curPage']] = r;
				 }
				 */
			} else {
				/*
				 var r = [];
				 r[0] = this.data.pageData[rowIndex];
				 this.options.savedSelectedRecords = {};
				 this.options.savedSelectedRecords[this.data.curPage] = r;
				 */
			}
		}
		if (this.options['actionCallback'] != undefined) {
			try {
				this.options['actionCallback'](flag, rowIndex, allChecked, this.data.pageData[rowIndex]);
			} catch (e) {
				log.error(e);
			}
		}
	},
	checkAll: function (flag) {
		var _self = this;
		$('#' + this.id + ' td input:checkbox[pagerFlag=true]').each(function () {
			$(this).attr('checked', flag);
			_self.fieldCheckChange(flag, $(this).attr('rowIndex'), true);
		});
//		this.options.savedSelectedRecords['page_'+this.data['curPage']] = this.data.pageData;
		if (this.options['actionCheckAllCallback'] != undefined) {
			try {
				this.options['actionCheckAllCallback'](flag);
			} catch (e) {
				log.error(e);
			}
		}
	},
	bindNavEvent: function () {
		var _self = this;
		$('#' + this.id + ' .pager a').each(function () {
			var pageNo = $(this).attr('pageIndex');
			$(this).bind('click', function () {
				_self.goToPage(pageNo);
			});
		});
		$('#' + this.id + '_perPageSelection').bind('change', function () {
			var perPage = $(this).val();
			if (_self.data.perPage != perPage) {
				_self.data.perPage = perPage;
				_self.data.curPage = 1;
				_self.refresh();
			}
		});
	},
	getPagerNavs: function () {
		var pages = this.data.totalPages;
		var cur = this.data.curPage;
		var navs = [];
		if (pages <= 12) {
			for (var i = 0; i < pages; i++) {
				navs[i] = i + 1;
			}
		} else {
			var seed = Math.floor((cur - 1) / 10);
			if ((seed + 1) * 10 >= pages) {
				var nav = seed * 10 + 1;
				while (nav <= pages) {
					navs[navs.length] = nav;
					nav++;
				}
			} else {
				for (var i = 0; i < 10; i++) {
					navs[i] = seed * 10 + 1 + i;
				}
				navs[navs.length] = '...';
				navs[navs.length] = pages - 1;
				navs[navs.length] = pages;
			}
		}
		return navs;
	},
	hasPreviousPage: function () {
		return (this.getPreviousPage() > 0);
	},
	hasNextPage: function () {
		return (this.getNextPage() <= this.data.totalPages);
	},
	getPreviousPage: function () {
		return (this.data.curPage - 1);
	},
	getNextPage: function () {
		return (this.data.curPage + 1);
	},
	goToPage: function (pageNo) {
		log.debug('want to go to ' + pageNo);
		pageNo = parseInt(pageNo);
		if (isNaN(pageNo) || pageNo > this.data.totalPages || pageNo < 1) {
			log.error('wrong page number');
		} else {
			this.data.curPage = pageNo;
			this.refresh();
		}
	},
	prepareLoadDataParameters: function () {
		var pagerParameters = {};
		pagerParameters['curPage'] = this.data.curPage;
		pagerParameters['perPage'] = this.data.perPage;
		pagerParameters['queryConditions'] = {};
		SolarVista.extend(pagerParameters['queryConditions'], this.options.parameters);
		SolarVista.extend(pagerParameters['queryConditions'], this.conditions);
		var sorts = [];
		if (this.currentSort != null) {
			for (var i = 0; i < this.currentSort.length; i++) {
				var cs = this.currentSort[i];
				if (cs['sort'] != 'none') {
					sorts[sorts.length] = {field: cs['field'], order: cs['order'].toLowerCase()};
				}
			}
		}
		for (var p in this.options.sorts) {
			sorts[sorts.length] = {field: p, order: this.options.sorts[p].toLowerCase()};
		}
		pagerParameters['sorts'] = sorts;
		return this.options.oldParameterMode ? pagerParameters : {pagerJson: JSON.stringify(pagerParameters)};
	},
	loadData: function () {
		var data = this.getDataFromCache(this.data.curPage);
		if (data != null) {
			log.debug('cache loading');
			this.loadDataCallback(data);
		} else {
			var _self = this;
			jQuery.ajax({
				url: this.options.loadDataURL,
				type: 'POST',
				data: this.prepareLoadDataParameters(),
				dataType: 'json',
				global: this.options.global,
				success: function (data) {
					_self.loadDataCallback(data);
				}
			});
		}
	},
	getDataFromCache: function (pageNo) {
		if (this.options.useClientCache) {
			if (this.cache['pageData_' + pageNo] != undefined) return this.cache['pageData_' + pageNo];
		}
		return null;
	},
	cachePageData: function (data) {
		if (this.options.useClientCache) this.cache['pageData_' + data['curPage']] = data;
	},
	loadDataCallback: function (response) {
		Loading.hide();
		log.debug(JSON.stringify(response));
		if (this.options.callbackBeforeRender) {
			log.trace("before render");
			if (null != this.options.loadDataCallback) {
				try {
					this.options.loadDataCallback(response);
				} catch (e) {
					log.error(e);
				}
			}
		}
		this.cachePageData(response);
		if (response.success) {
			this.data = response;
			if (this.data.pageData == null) this.data.pageData = [];
			this.showPager(false);
		} else {
			if (response.message == null) response.message = '未知错误，未能查询到数据';
			MSG.error(response.message, true);
			this.showDataLoadingError(response.message);
		}
		if (!this.options.callbackBeforeRender) {
			log.trace("after render");
			if (null != this.options.loadDataCallback) {
				try {
					this.options.loadDataCallback(response);
				} catch (e) {
					log.error(e);
				}
			}
		}
	},
	showDataLoadingError: function (s) {
		MSG.error(s, true);
	},
	setSearchCondition: function (field, value) {
		if (value == undefined) value = $('#' + field).val();
		var ev = this.conditions[field];
		if (ev == value) return;
		this.conditions[field] = value;
		this.data.curPage = 1;
		this.cache = {};
	},
	setSortField: function (field, order) {
		if (order == undefined) order = 'Asc';
		order = order.toLowerCase();
		if (order == 'asc') order = 'Asc';
		else if (order == 'desc') order = 'Desc';
		else order = 'none';
		var eo = this.sorts[field];
		if (eo == order) return;
		this.sorts[field] = order;
		if (this.currentSort == null) this.currentSort = [];
		var found = false;
		for (var i = 0; i < this.currentSort.length; i++) {
			var sort = this.currentSort[i];
			if (sort['field'] == field) {
				found = true;
				if (order == 'none') {
					this.currentSort.splice(i, 1);
				} else {
					sort['order'] = order;
				}
				break;
			}
		}
		if (!found) {
			this.currentSort[this.currentSort.length] = {field: field, order: order};
		}
		this.data.curPage = 1;
		this.cache = {};
	},
	refresh: function () {
		this.loadData();
	},
	getSelectionRecords: function () {
		var result = this.options.savedSelectedRecords['page_' + this.data.curPage];
		if (result == undefined) result = [];
		return result;
	},
	clearConditions: function () {
		this.conditions = {};
		this.options.savedSelectedRecords = [];
	},
	clearSorts: function () {
		this.sorts = {};
		this.currentSort = [];
	},
	getPageData: function () {
		return this.data.pageData;
	},
	getFilteredData: function () {
		var result = [];
		for (var i = 0; i < this.data.pageData.length; i++) {
			if (this.options.filter(this.data.pageData[i])) {
				result[result.length] = this.data.pageData[i];
			}
		}
		return result;
	},
	getSavedSelectedRecords: function () {
		if (this.options.saveSelectedRecords) {
			var result = [];
			for (var page in this.options.savedSelectedRecords) {
				result = result.concat(this.options.savedSelectedRecords[page]);
			}
			return result;
		}
		else return [];
	},
	removeSavedSelectedRecord: function (pk) {
		if (this.options.saveSelectedRecords) {
			for (var page in this.options.savedSelectedRecords) {
				var pageData = this.options.savedSelectedRecords[page];
				for (var i = 0; i < pageData.length; i++) {
					if (pageData[i][this.options.pkField] == pk) {
						pageData.splice(i, 1);
						this.options.savedSelectedRecords[page] = pageData;
						return true;
					}
				}
			}
		}
		return false;
	},
	getSavedSelectedRecord: function (pk) {
		if (this.options.saveSelectedRecords) {
			var allSaved = this.getSavedSelectedRecords();
			for (var i = 0; i < allSaved; i++) {
				if (allSaved[i][this.options.pkField] == pk) return allSaved[i];
			}
			return null;
		} else {
			return null;
		}
	},

	showColumnsSelector: function () {
		var html = '';
		html += '<div class="pager">';
		for (var columnIndex = 0; columnIndex < this.options.columns.length; columnIndex++) {
			var column = this.options.columns[columnIndex];
			html += '<span style="margin-right: 10px;cursor: pointer;" name="' + this.id + 'columnselector" tag="' + columnIndex + '" ><input ' + (column['show'] ? 'checked="checked"' : '') + ' name="columnItemSelector" type="checkbox" />' + column['title'] + '</span>';
		}
		html += '</div>';
		return html;
	},

	bindColumnSelectorEvent: function () {
		var selector = this.id + 'columnselector';
		var _this = this;
		$('span[name="' + selector + '"]').each(function () {
			$(this).bind('click', function () {
				_this.onChangeShowColumn($(this).attr('tag'));
			});
		});
	},

	onChangeShowColumn: function (columnIndex) {
		log.debug('aaaa' + columnIndex);
		var column = this.options.columns[columnIndex];
		column['show'] = !column['show'];
		this.refresh();
//        this.showPager(false);
	}
};

if (typeof(pager) == 'undefined') {
	pager = LP3.Pager;
}