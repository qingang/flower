(function(a) {
    a.fn.Jlazyload = function(j, n) {
        if (!this.length) {
            return

        }
        var f = a.extend({
            type: null,
            offsetParent: null,
            source: "data-lazyload",
            placeholderImage: "../image/loading.gif",
            placeholderClass: "image-loading",
            threshold: 200

        },
        j || {}),
        k = this,
        g,
        m,
        l = function(r) {
            var u = r.scrollLeft,
            t = r.scrollTop,
            s = r.offsetWidth,
            q = r.offsetHeight;
            while (r.offsetParent) {
                u += r.offsetLeft;
                t += r.offsetTop;
                r = r.offsetParent

            }
            return {
                left: u,
                top: t,
                width: s,
                height: q

            }

        },
        e = function() {
            var v = document.documentElement,
            r = document.body,
            u = window.pageXOffset ? window.pageXOffset: (v.scrollLeft || r.scrollLeft),
            t = window.pageYOffset ? window.pageYOffset: (v.scrollTop || r.scrollTop),
            s = v.clientWidth,
            q = v.clientHeight;
            return {
                left: u,
                top: t,
                width: s,
                height: q

            }

        },
        d = function(w, v) {
            var y,
            x,
            s,
            r,
            q,
            u,
            z = f.threshold ? parseInt(f.threshold) : 0;
            y = w.left + w.width / 2;
            x = v.left + v.width / 2;
            s = w.top + w.height / 2;
            r = v.top + v.height / 2;
            q = (w.width + v.width) / 2;
            u = (w.height + v.height) / 2;
            return Math.abs(y - x) < (q + z) && Math.abs(s - r) < (u + z)

        },
        b = function(q, s, r) {
            if (f.placeholderImage && f.placeholderClass) {
                r.attr("src", f.placeholderImage).addClass(f.placeholderClass)

            }
            if (q) {
                r.attr("src", s).removeAttr(f.source);
                if (n) {
                    n(s, r)

                }

            }

        },
        c = function(q, t, r) {
            if (q) {
                var s = a("#" + t);
                s.html(r.val()).removeAttr(f.source);
                r.remove();
                if (n) {
                    n(t, r)

                }

            }

        },
        p = function(q, s, r) {
            if (q) {
                r.removeAttr(f.source);
                if (n) {
                    n(s, r)

                }

            }

        },
        o = function() {
            m = e(),
            k = k.filter(function() {
                return a(this).attr(f.source)

            });
            a.each(k, 
            function() {
                var t = a(this).attr(f.source);
                if (!t) {
                    return

                }
                var s = (!f.offsetParent) ? m: l(a(f.offsetParent).get(0)),
                r = l(this),
                q = d(s, r);
                switch (f.type) {
                    case "image":
                    b(q, t, a(this));
                    break;
                    case "textarea":
                    c(q, t, a(this));
                    break;
                    case "module":
                    p(q, t, a(this));
                    break;
                    default:
                    break

                }

            })

        },
        h = function() {
            if (k.length > 0) {
                clearTimeout(g);
                g = setTimeout(function() {
                    o()

                },
                10)

            }

        };
        o();
        if (!f.offsetParent) {
            a(window).bind("scroll", 
            function() {
                h()

            }).bind("reset", 
            function() {
                h()

            })

        } else {
            a(f.offsetParent).bind("scroll", 
            function() {
                h()

            })

        }

    }

})(jQuery);


(function() {
    $("img[data-lazyload]").Jlazyload({
        type: "image"
    });

})();


