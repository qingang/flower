<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.contant.Global"%>
<div class="index_car_box" style="padding:8px;">
	<p class="course_category_title_01">
		<a href="links-${mtype}.html" class="pull_right"><img src="${ctx}/image/index_btn_more.png" /></a>${typeName.label} 
	</p>
	<ul>
		<s:iterator value="linksList" status="st">
			<li>
				<s:if test="linkurl!=null && linkurl!=''">
			    	<a target="_blank" href="${linkurl}">
						<img  data-lazyload="${ctx}<%=Global.picpath%>${picurl}" width="117px" height="117px"/>
			        </a>
		    	</s:if>
			    <s:else>
			        <a href="links-${type}-${id}.html" target="_blank">
			        	<img  data-lazyload="${ctx}<%=Global.picpath%>${picurl}" width="117px" height="117px"/>
			        </a>
			    </s:else>
			    <p>${title}</p>
		    </li>
	    </s:iterator>
	</ul> 
</div>