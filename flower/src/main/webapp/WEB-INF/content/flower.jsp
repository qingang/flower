<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.contant.Global"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
	<title>短平快-汽车服务-汽车玻璃修补-花卉预定-${typeName.label}</title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/css/front.css" rel="stylesheet" type="text/css" /> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">	
		<%@ include file="/common/header.jsp" %>
		<div id="content">
			<div class="title">
 				<div class="icon"></div>
 				<ul><li class="ll">花卉展示</li></ul>
 			</div>
 			<form id="mainForm" name="mainForm" method="post" action="flower.html">
			<div class="index_car_box">
				<ul>
					<s:iterator value="page.result">
						<li title="${title}">
	                    	<s:if test="linkurl!=null && linkurl!=''">
	                    		<a href="${linkurl}" target="_blank" class="ellipsis">
                               		<img width="130px" border="0" height="130px" data-lazyload="${ctx}<%=Global.picpath%>${picurl}"/> 
                               	</a>
	                    	</s:if>
	                    	<s:else>
                                <a href="flower-${id}.html" target="_blank" class="ellipsis">
                                	<img width="130px" border="0" height="130px" data-lazyload="${ctx}<%=Global.picpath%>${picurl}"/> 
                                </a>
                            </s:else>
                            <p>${title}</p>
		                 </li>
	                 </s:iterator>
		    	</ul>
			</div>
			<%@ include file="/common/frontpage.jsp"%>
		   </form>

		</div>
		<%@ include file="/common/footer.jsp" %>
	</div>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>
