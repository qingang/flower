<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<div class="index_news_2box">
	<div class="index_news" style="padding:8px;">
		<p class="course_category_title_01">
			${typeName.label} 
		</p>
		<ul>
			<s:iterator value="linksList">
				<li>
					<span class="rr"><s:date name="publishdate" format="yyyy.MM.dd"/></span>
					<a href="${linkurl}" target="_blank" title="${title}">
						<common:cut len="30" string="${title}"/>
					</a>
				</li>
			</s:iterator>
		</ul> 
	</div>
</div>