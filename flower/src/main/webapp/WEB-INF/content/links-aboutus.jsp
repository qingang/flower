<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.contant.Global"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
	<title>短平快-汽车服务-汽车玻璃修补-花卉预定-关于我们</title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/css/front.css" rel="stylesheet" type="text/css" /> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">	
		<%@ include file="/common/header.jsp" %>
		<div id="content">
			<div class="title">
 				<div class="icon"></div>
 				<ul>
 					<li class="ll">关于我们</li>
 				</ul>
 			</div>
			<div class="detailbox">
        	
				
				<span class="news_details_con"> 
					${entity.remark}	
        		</span>
      		</div>
			<p class="clearb"></p>
		</div>
		<%@ include file="/common/footer.jsp" %>
	</div>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>
