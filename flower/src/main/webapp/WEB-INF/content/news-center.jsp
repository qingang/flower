<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<div class="index_news_2box" style="margin-right:9px;">
	<div class="index_news" style="padding:8px;">
		<p class="course_category_title_01">
			<a href="news-${mtype}.html" class="pull_right"><img src="${ctx}/image/index_btn_more.png" /></a>新闻中心 
		</p>
		<ul>
			<s:iterator value="newsList">
				<li>
					<span class="rr"><s:date name="publishdate" format="yyyy.MM.dd"/></span>
					<s:if test="link!=null && link!=''">
                   		<a href="${link}" target="_blank" title="${title}">
                           <common:cut len="30" string="${title}"/> 
                        </a>
                   	</s:if>
                   	<s:else>
                        <a href="news-${type}-${id}.html" title="${title}" target="_blank">
                        	<common:cut len="30" string="${title}"/> 
                        </a>
                    </s:else>
				</li>
			</s:iterator>
		</ul> 
	</div>
</div>