<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.contant.Global"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
	<title>短平快-汽车服务-汽车玻璃修补-花卉预定</title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/css/front.css" rel="stylesheet" type="text/css" /> 
	<link href="${ctx}/js/validate/jquery.validate.css" type="text/css" rel="stylesheet"/>
	<link rel="stylesheet" href="${ctx}/KindEditor/themes/default/default.css" />
	<link rel="stylesheet" href="${ctx}/KindEditor/plugins/code/prettify.css" />
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/jquery.validate.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/messages_cn.js" type="text/javascript"></script>
	<script type="text/javascript" src="${ctx}/KindEditor/kindeditor-min.js"></script>
	<script type="text/javascript" src="${ctx}/KindEditor/lang/zh_CN.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		//聚焦第一个输入框
		$("#code").focus();
		$("#inputForm").validate({
			rules: {
				code: {
					required:true
				},
				title: {
					required:true
				}
			},
			messages: {
				code: {
					required: "请输入验证码"
				},
				title: {
					required: "请输入留言标题"
				}
			},
	        errorPlacement: function(error, element) {   
		        if (document.getElementById("error_"+element.attr("name")))  {
		            error.appendTo("#error_"+element.attr("name"));  
		        }
		        else       
		            error.insertAfter(element);   
		        },
		        errorElement: "em"
		}); 
	});
	var htmlEditor = null;
	KindEditor.ready(function(K) {
		htmlEditor = K.create('textarea[name=remark]', {
			cssPath : '${ctx}/KindEditor/themes/default/default.css',
			uploadJson : '${ctx}/KindEditor/jsp/upload_json.jsp',
			fileManagerJson : '${ctx}/KindEditor/jsp/file_manager_json.jsp',
			allowFileManager : true	//允许查看文件
		});
	});
	function onSubmit() {
		 if($("#inputForm").valid()){
		 if(!htmlEditor.isEmpty()){
		 	$("#remark").val(htmlEditor.html());
		 	$("#inputForm").submit();
		 }	else{
		 	alert("留言内容不能为空");
		 }
		}
	}
	document.onkeydown = function(e){    
		e = e || window.event;   
		if(e.keyCode === 13){        
		 onSubmit();   
		}
	};
	function changeimg(){  
		var myimg = document.getElementById("imgcode");  
	    myimg.src="authImg?"+Math.random();//随机生成一个数字，让图片缓冲区认为不是同一个缓冲区  
	}


</script>
</head>
<body>
	<div id="wrapper">	
		<%@ include file="/common/header.jsp" %>
		<div id="content">
			<div class="title">
 				<div class="icon"></div>
 				<ul>
 					<li class="ll">花卉预定</li>
 				</ul>
 			</div>
			<div class="message_box">
				<div id="message" style="line-height:20px;">
					<s:actionmessage theme="custom" cssClass="alert alert-success"/>
				</div>
      			<form method="post" action="order!save.action" name="inputForm" id="inputForm">
	        		<ul class="form_list">
						<li>以下有<b>*</b>的内容为必填项：</li>
						<li>
	            			<label>验证码<b>*</b></label>
	            			<input type="text" size="10" value="" name="code" id="code"/>
	            			<img id="imgcode" src="authImg" border="0" align="top" onclick="changeimg();" style='cursor: pointer;' title="看不清,请点击!" />
	            			<span id="error_code"></span>
	          			</li>
	          			<li>
	            			<label>标题<b>*</b></label>
	            			<input type="text" size="40" value="" name="title" id="title"/><span id="error_title"></span>
	          			</li>
						<li>
							<label style="vertical-align: top;">预定内容<b>*</b></label>
							<textarea id="remark" name="remark" cols="100" rows="8" style="width:750px;height:300px;visibility:hidden;">${remark}</textarea>
		        			<span id="error_remark"></span>
						</li>

	        		</ul>
	        		<p class="button_box"><button type="button" onclick="onSubmit();" class="button01">提交</button></p>
        </form>
     </div>
			<p class="clearb"></p>
		</div>
		<%@ include file="/common/footer.jsp" %>
	</div>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>
