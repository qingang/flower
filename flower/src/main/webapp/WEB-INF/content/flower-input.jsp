<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.contant.Global"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
	<title>短平快-汽车服务-汽车玻璃修补-花卉预定-${title}</title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/css/front.css" rel="stylesheet" type="text/css" /> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">	
		<%@ include file="/common/header.jsp" %>
		<div id="content">
			<div class="title">
 				<div class="icon"></div>
 				<ul>
 					<li class="ll">花卉展示</li>
 					<li class="ll">&gt;&gt;</li>
					<li class="ll">${title}</li>
 				</ul>
 			</div>
			<div class="detailbox">
        		<div class="newstitle">
          			<p class="news_fonttitle style_newstitle_color">${title}</p>
          			<p class="summary">发布时间：<s:date name="publishdate" format="yyyy.MM.dd HH:mm"/></p>
					<div style="bottom: 0;overflow: hidden;float:right;">
				 		<!-- Baidu Button BEGIN -->
						<div id="bdshare" class="bdshare_t bds_tools get-codes-bdshare">
							<a class="bds_qzone"></a>
							<a class="bds_tsina"></a>
							<a class="bds_tqq"></a>
							<a class="bds_renren"></a>
							<a class="bds_t163"></a>
							<span class="bds_more">更多</span>
						</div>
						<script type="text/javascript" id="bdshare_js" data="type=tools&amp;uid=0" ></script>
						<script type="text/javascript" id="bdshell_js"></script>
						<script type="text/javascript">
						document.getElementById("bdshell_js").src = "http://bdimg.share.baidu.com/static/js/shell_v2.js?cdnversion=" + Math.ceil(new Date()/3600000)
						</script>
						<!-- Baidu Button END -->
			 		</div>
        		</div>
        		<img width="130px" border="0" height="130px" data-lazyload="${ctx}<%=Global.picpath%>${picurl}"/>
				<p class="clearb"></p>
				
				<span class="news_details_con"> 
					${remark}	
        		</span>
      		</div>
			<p class="clearb"></p>
		</div>
		<%@ include file="/common/footer.jsp" %>
	</div>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>
