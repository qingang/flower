<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
	<title>短平快汽车玻璃-汽车服务-汽车玻璃修补-花卉预定</title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/css/front.css" rel="stylesheet" type="text/css" /> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
</head>
<body>
	<div id="identifier-pannel" style="display: block; top: 10px;">
		<a target="_blank" title="短片快汽车玻璃" href="http://www.qcbl1688.com">
			二维码名片
			<img width="100" height="100" src="${ctx}/image/f-app.png"/>
		</a>
	</div>
	<div id="wrapper">	
		<%@ include file="/common/header.jsp" %>
		<div>
			<div id="imgplayer"   style="display:none;">
				<img data-lazyload ="${ctx}/image/index-1.jpg" />
				<img data-lazyload ="${ctx}/image/index-2.jpg" />
				<img data-lazyload ="${ctx}/image/index-3.jpg" />
			</div>
		</div>
		<div id="content">
			<div class="focus_block">
				<c:import url="${appurl}/news!technology.excsec?mtype=1&num=6"/>
				<c:import url="${appurl}/news!center.excsec?mtype=0&num=6"/>
				<c:import url="${appurl}/links!friend.excsec?ltype=1&num=6"/>
				<c:import url="${appurl}/flower!index.excsec?num=8"/>
			</div>
			<div class="index_announce">
				<c:import url="${appurl}/links!left.excsec?ltype=3&num=6"/>
				<c:import url="${appurl}/links!left.excsec?ltype=2&num=6"/>
			</div>
		</div>
		<%@ include file="/common/footer.jsp" %>
	</div>
	<script type="text/javascript" src="js/common.js"></script>
	<script type="text/javascript" src="js/jquery.fn.imgplayer.js"></script>
	<script type="text/javascript">
		$("#imgplayer").player({
			width	: '989px',
			height  : '307px',
			showTitle : false,
			showIndex:false
		}).play();

	</script>
</body>
</html>
