<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
	<title>短平快-汽车服务-汽车玻璃修补-花卉预定-${typeName.label}</title>
	<%@ include file="/common/meta.jsp" %>
	<link href="${ctx}/css/front.css" rel="stylesheet" type="text/css" /> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">	
		<%@ include file="/common/header.jsp" %>
		<div id="content">
			<div class="title">
 				<div class="icon"></div>
 				<ul><li class="ll">${typeName.label}</li></ul>
 			</div>
			<div class="newslist">
			<form id="mainForm" name="mainForm" method="post" action="news-${mtype}.html">
				<ul>
					<s:iterator value="page.result">
		            	<li title="${title}">
		                	<span class="rr">
		                    	<s:date name="publishdate" format="yyyy-MM-dd"/>
		                    </span>
		                    <span class="ntitle">
		                    	<s:if test="link!=null && link!=''">
		                    		<a href="${link}" target="_blank" class="ellipsis">
                                		<common:cut len="50" string="${title}"/> 
                                	</a>
		                    	</s:if>
		                    	<s:else>
	                                <a href="news-${type}-${id}.html" target="_blank" class="ellipsis">
	                                	<common:cut len="50" string="${title}"/> 
	                                </a>
                                </s:else>
                            </span>
		                 </li>
	                 </s:iterator>
		    	</ul>
		    	 <%@ include file="/common/frontpage.jsp"%>
		    	</form>
			</div>
			<div class="index_announce">
				<c:import url="${appurl}/links!left.excsec?ltype=3&num=6"/>
				<c:import url="${appurl}/links!left.excsec?ltype=2&num=6"/>
			</div>
		</div>
		<%@ include file="/common/footer.jsp" %>
	</div>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>
