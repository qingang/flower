<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.contant.Global"%>
<p class="clearb"></p>
<div class="index_news">
	<p class="course_category_title_01">
		<a href="flower.html" class="pull_right"><img src="${ctx}/image/index_btn_more.png" /></a>花卉展示
	</p>
	<ul class="class_box_new">
		<s:iterator value="flowerList" status="st">
			<li title="${title}">
				<s:if test="linkurl!=null && linkurl!=''">
			    	<a target="_blank" href="${linkurl}">
						<img  data-lazyload="${ctx}<%=Global.picpath%>${picurl}" width="110px" height="110px"/>
			        </a>
		    	</s:if>
			    <s:else>
			        <a href="flower-${id}.html" target="_blank">
			        	<img  data-lazyload="${ctx}<%=Global.picpath%>${picurl}" width="110px" height="110px"/>
			        </a>
			    </s:else>
			    <p><common:cut len="25" string="${title}"/></p>
		    </li>
	    </s:iterator>
	</ul>
</div>