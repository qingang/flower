<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.util.CommonUtils"%>
<%@page import="com.gm.flower.contant.Global"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%@ include file="/common/meta.jsp" %>
	<title>短平快网站后台-关于我们</title>
	<meta content="关于我们" name="activemenu" />
	<link href="${ctx}/js/validate/jquery.validate.css" type="text/css" rel="stylesheet"/>
	<link href="${ctx}/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/> 
	<link href="${ctx}/css/backend.css" type="text/css" rel="stylesheet"/> 
	<link rel="stylesheet" href="${ctx}/KindEditor/themes/default/default.css" />
	<link rel="stylesheet" href="${ctx}/KindEditor/plugins/code/prettify.css" />
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/jquery.validate.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/messages_cn.js" type="text/javascript"></script>
	<script type="text/javascript" src="${ctx}/KindEditor/kindeditor-min.js"></script>
	<script type="text/javascript" src="${ctx}/KindEditor/lang/zh_CN.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		//聚焦第一个输入框
		$("#title").focus();
		$("#inputForm").validate({
			rules: {
	
			},
			messages: {

			},
	        errorPlacement: function(error, element) {   
		        if (document.getElementById("error_"+element.attr("name")))  {
		            error.appendTo("#error_"+element.attr("name"));  
		        }
		        else       
		            error.insertAfter(element);   
		        },
		        errorElement: "em"
		}); 
	});
	var htmlEditor = null;
	KindEditor.ready(function(K) {
		htmlEditor = K.create('textarea[name=remark]', {
			cssPath : '${ctx}/KindEditor/themes/default/default.css',
			uploadJson : '${ctx}/KindEditor/jsp/upload_json.jsp',
			fileManagerJson : '${ctx}/KindEditor/jsp/file_manager_json.jsp',
			allowFileManager : true	//允许查看文件
		});
	});
	function onSubmit() {
		 if($("#inputForm").valid()){
		 if(!htmlEditor.isEmpty()){
		 	$("#remark").val(htmlEditor.html());
		 	$("#inputForm").submit();
		 }	else{
		 	alert("内容不能为空");
		 }
		}
	}
	document.onkeydown = function(e){    
		e = e || window.event;   
		if(e.keyCode === 13){        
		 onSubmit();   
		}
	};


</script>
</head>
<body>
<div class="wrapper">
		<%@ include file="/common/adminheader.jsp"%>
		<div class="m010">
        <!--导航开始-->
        	<ul class="breadcrumb">
            	<li><a href="${ctx}/backend/index.action"><i class="icon-home ml10"></i>&nbsp;</a></li>
                <li class="divider"></li>
               	<li>辅助管理</li>
               	<li class="divider"></li>
               	<li>关于我们</li>
                <li class="last-divider"></li>
        	</ul>
        	<!--导航结束-->
        	<div id="message" style="line-height:20px;">
				<s:actionmessage theme="custom" cssClass="alert alert-success"/>
			</div>
        	<form class="form-horizontal form-group" id="inputForm" name="inputForm" action="aboutus!save.action" method="post">
	   			<input type="hidden" id="id" name="id" value="${us.id}"/>
                  <fieldset>
                      <div class="control-group">
                          <label for="input01" class="control-label" style="vertical-align: top;">内容<b>*</b></label>
                          <div class="controls">
                              <textarea id="remark" name="remark" cols="100" rows="8" style="width:750px;height:500px;visibility:hidden;">${us.remark}</textarea>
		        			<span id="error_remark"></span>
                          </div>
                      </div>
                      <div class="btn-center-box form-actions">
                          <button class="btn btn-primary btn-large mr10" type="button" onclick="onSubmit();"><i class="icon-ok icon-white"></i>保 存</button>
                      </div>
                  </fieldset>
            </form>
			</div>
		</div>
</body>
</html>