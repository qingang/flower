<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.contant.Global"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%@ include file="/common/meta.jsp" %>
	<title>短平快网站后台-花卉订单</title>
	<meta content="花卉订单" name="activemenu" />
	<link href="${ctx}/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/> 
	<link href="${ctx}/css/backend.css" type="text/css" rel="stylesheet"/> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
	<script src="${ctx}/js/jcommon.js" type="text/javascript"></script>
	<script  type="text/javascript">
	$(document).ready(function() {
		$("#checkall").click(function(){
 			$("input[name='ids']").attr("checked",$(this).attr("checked"));
 		});
	});

	function onPublish(){
		var  isChecked=false;
		$("input[name='ids']:checked").each(function(){
			isChecked = true;
		});
		if(!isChecked){
			alert("选择标记的记录");
			return false;
		}

		if(confirm("确认标记已读？")){
			$("#mainForm").attr("action","order!publish.action").submit();
		}
	}
	function onCancel(){
		var  isChecked=false;
		$("input[name='ids']:checked").each(function(){
			isChecked = true;
		});
		if(!isChecked){
			alert("选择取消标记的记录");
			return false;
		}

		if(confirm("确认标记未读？")){
			$("#mainForm").attr("action","order!cancelPublish.action").submit();
		}
	}
	
	</script>
</head>
<body> 
		<div class="wrapper"> 
		<%@ include file="/common/adminheader.jsp"%>
		<div class="m010">
        	<!--导航开始-->
            <ul class="breadcrumb">
            	<li><a href="${ctx}/backend/index.action"><i class="icon-home ml10"></i>&nbsp;</a></li>
                    <li class="divider"></li>
                    <li>花卉管理</li>
                    <li class="divider"></li>
                    <li>订单中心</li>
                    <li class="last-divider"></li>
                </ul>
                <!--导航结束--><!--form box start-->
             	<div id="order" style="line-height:20px;">
					<s:actionmessage theme="custom" cssClass="alert alert-success"/>
				</div>
                <form id="mainForm" action="order.action" class="well form-inline" method="post">
                	<input type="hidden" name="page.pageNo" id="pageNo" value="${page.pageNo}"/>
					<input type="hidden" name="page.orderBy" id="orderBy" value="${page.orderBy}"/>
					<input type="hidden" name="page.order" id="order" value="${page.order}"/>
                    <span>
                    	<label>标题</label>
						<input type="text" id="title" name="filter_LIKES_title"  value="${param['filter_LIKES_title']}"/>
                    </span>
                    <span>
                    	<label>状态</label>
                        <s:select list="#{'true':'已 读','false':'未 读'}"  id="ispublic" name="filter_EQB_ispublic"  value="#parameters.filter_EQB_ispublic" cssClass="span2" cssStyle="width:85px;" headerKey="" headerValue="--全 部--"/> 
                    </span>
                    <button class="btn" type="submit"><i class="icon-search"></i></button>
               
                <div class="tab_btn_toolbar">
	                <div class="btn-toolbar">
	         			<div class="btn-group">
	                    	<!--<button class="btn" id="btnCreate" type="button" onclick="window.location.href='order!input.action'"><i class="icon-plus"></i>&nbsp;新增</button>  -->
		    				<button class="btn" id="btnPublish" type="button" onclick="onPublish();"><i class="icon-share"></i>&nbsp;标记已读</button>
		    				<button class="btn" id="btnCancel" type="button" onclick="onCancel();"><i class="icon-remove"></i>&nbsp;标记未读</button>
	                    </div>
	                </div>
                	<div class="datatables-wrapper">
                    	<table class="table table-bordered">
                        	<thead>
                            	<tr>
							    	<th width="5%"><input type="checkbox" id="checkall" name="checkall"/></th>
							        <th width="50%">标题</th>
							        <th width="20%">下单时间</th>
							        <th width="10%">是否发布</th>
							        <th width="15%">操作</th>
							    </tr>
                        	</thead>
                        	<s:iterator value="page.result" status="st">
							   <tr>
							   		<td title=""><input type="checkbox" id="ids" name="ids" value="${id}"/></td>
							   		<td title="${title}"><common:cut string="${title}" len="40"/></td>
							        <td><s:date name="createdate" format="yyyy-MM-dd HH:mm"/></td>
							        <td>${statusName}</td>
							        <td>
							        	<a href="order!input.action?id=${id}">查 看</a>
							        </td>
							    </tr>
						    </s:iterator>
						    <s:if test="page.result.size ==0">
								<tr> 
									<td height="30" align="center" colspan="5"><font color="gray">没有符合条件的记录</font></td>
								</tr>  
							</s:if> 
                    	</table>
                    	<%@ include file="/common/backendpage.jsp"%>
                	</div>
                </div>
                 </form>
               
			</div>
		</div>
</body>
</html>