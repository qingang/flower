<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%@ include file="/common/meta.jsp" %>
	<title>网站后台-修改密码</title>
	<link href="${ctx}/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/> 
	<link href="${ctx}/css/backend.css" type="text/css" rel="stylesheet"/> 
	<link href="${ctx}/js/validate/jquery.validate.css" type="text/css" rel="stylesheet"/>
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/jquery.validate.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/messages_cn.js" type="text/javascript"></script>
		<script type="text/javascript" >
		$(document).ready(function() { 
		//聚焦第一个输入框
		$("#inputForm").validate({
			rules: {
				oldpass: {
					required: true,
					equalTo: "#opass"
				},
				password: {
					required: true
				},
				qpassword: {
					required: true,
					equalTo: "#password"
				}
			},
			messages: {
				oldpass: {
					required: "输入当前密码",
					equalTo: "当前密码输入有误"
				},
				password: {
					required: "输入新密码"
				},
				qpassword: {
					required: "输入确认密码",
					equalTo: "两次密码不相同"
				}
			},errorPlacement: function(error, element) {   
 		        if (document.getElementById("error_"+element.attr("name")))  {
 		            error.appendTo("#error_"+element.attr("name"));  
 		        }else       
 		            error.insertAfter(element);   
 		        },
 		       errorElement: "em"
			});	
	});
		</script>
</head>
<body>
	<div class="wrapper">
		<%@ include file="/common/adminheader.jsp"%>
		<div class="m010">
        <!--导航开始-->
        	<ul class="breadcrumb">
            	<li><a href="${ctx}/backend/index.action"><i class="icon-home ml10"></i>&nbsp;</a></li>
                <li class="divider"></li>
               	<li>修改密码</li>
                <li class="last-divider"></li>
        	</ul>
        <!--导航结束-->
                <div id="message" style="line-height:20px;">
					<s:actionmessage theme="custom" cssClass="alert alert-success"/>
				</div>
                <form class="form-horizontal form-group" id="inputForm" name="inputForm" action="user!savePass.action" method="post">
			   		<input id="id" name="id" type="hidden" value="${entity.id}" size="30"/>
			   		<input id="opass" name="opass" type="hidden" value="${entity.password}" size="30"/>
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label">登录名</label>
                            <div class="controls mt5">
                                ${entity.loginName}
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="input01" class="control-label">当前密码<b>*</b></label>
                            <div class="controls">
                                <input  id="oldpass" name="oldpass"  type="password"  />
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="input01" class="control-label">新密码<b>*</b></label>
                            <div class="controls">
                                <input  id="password" name="password" type="password" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="input01" class="control-label">确认密码<b>*</b></label>
                            <div class="controls">
                                <input  id="qpassword" name="qpassword"  type="password"  />
                            </div>
                        </div>
                        <div class="btn-center-box form-actions">
                            <button class="btn btn-primary btn-large mr10" type="submit"><i class="icon-ok icon-white"></i>保存</button>
                            <!--<a class="btn btn-large mr10" href="#"><i class="icon-repeat"></i>&nbsp;还原</a>  -->
                        </div>
                    </fieldset>
                </form>
            </div>
 		</div>
</body>
</html>