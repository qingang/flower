<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="com.gm.flower.util.CommonUtils"%>
<%@page import="com.gm.flower.contant.Global"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%@ include file="/common/meta.jsp" %>
	<title>短平快网站后台-${typeName.label}</title>
	<meta content="${typeName.value}" name="activemenu" />
	<link href="${ctx}/js/validate/jquery.validate.css" type="text/css" rel="stylesheet"/>
	<link href="${ctx}/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/> 
	<link href="${ctx}/css/backend.css" type="text/css" rel="stylesheet"/> 
	<link rel="stylesheet" href="${ctx}/KindEditor/themes/default/default.css" />
	<link rel="stylesheet" href="${ctx}/KindEditor/plugins/code/prettify.css" />
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/jquery.validate.js" type="text/javascript"></script>
	<script src="${ctx}/js/validate/messages_cn.js" type="text/javascript"></script>
	<script type="text/javascript" src="${ctx}/KindEditor/kindeditor-min.js"></script>
	<script type="text/javascript" src="${ctx}/KindEditor/lang/zh_CN.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		//聚焦第一个输入框
		$("#title").focus();
		$("#inputForm").validate({
			rules: {
				title: {
					required:true
				},
				linkurl: {
					required:true
				}
			},
			messages: {
				title: {
					required: "请输入标题"
				},
				linkurl: {
					required: "请输入外部链接"
				}
			},
	        errorPlacement: function(error, element) {   
		        if (document.getElementById("error_"+element.attr("name")))  {
		            error.appendTo("#error_"+element.attr("name"));  
		        }
		        else       
		            error.insertAfter(element);   
		        },
		        errorElement: "em"
		}); 
	});
	var htmlEditor = null;
	KindEditor.ready(function(K) {
		htmlEditor = K.create('textarea[name=remark]', {
			cssPath : '${ctx}/KindEditor/themes/default/default.css',
			uploadJson : '${ctx}/KindEditor/jsp/upload_json.jsp',
			fileManagerJson : '${ctx}/KindEditor/jsp/file_manager_json.jsp',
			allowFileManager : true	//允许查看文件
		});
	});
	function onSubmit() {
		 if($("#inputForm").valid()){
		 if(!htmlEditor.isEmpty()){
		 	$("#remark").val(htmlEditor.html());
		 	$("#inputForm").submit();
		 }	else{
		 	alert("内容不能为空");
		 }
		}
	}
	document.onkeydown = function(e){    
		e = e || window.event;   
		if(e.keyCode === 13){        
		 onSubmit();   
		}
	};


</script>
</head>
<body>
<div class="wrapper">
		<%@ include file="/common/adminheader.jsp"%>
		<div class="m010">
        <!--导航开始-->
        	<ul class="breadcrumb">
            	<li><a href="${ctx}/backend/index.action"><i class="icon-home ml10"></i>&nbsp;</a></li>
                <li class="divider"></li>
               	<li>链接管理</li>
               	<li class="divider"></li>
               	<li>${typeName.label}</li>
               	<li class="divider"></li>
               	<li>
               		<s:if test="id!=null">编辑</s:if><s:else>新增</s:else>
               	</li>
                <li class="last-divider"></li>
        	</ul>
        <!--导航结束-->
        	<form class="form-horizontal form-group" id="inputForm" name="inputForm" action="friendlink!save.action?mtype=${mtype}" method="post">
	   			<input type="hidden" id="id" name="id" value="${id}"/>
                  <fieldset>
                      <div class="control-group">
                          <label class="control-label">标题<b>*</b></label>
                          <div class="controls">
                              <input id="title" name="title" value="${title}" type="text" maxlength="100" style="width:300px;" />
                          </div>
                      </div>
                      
                      <div class="control-group">
                          <label  class="control-label">外部链接<b>*</b></label>
                          <div class="controls">
                              <input id="linkurl" name="linkurl" value="${linkurl}" type="text" maxlength="100" style="width:300px;" />
                          </div>
                      </div>
                      <div class="control-group">
                          <label for="input01" class="control-label" style="vertical-align: top;">内容<b>*</b></label>
                          <div class="controls">
                              <textarea id="remark" name="remark" cols="100" rows="8" style="width:750px;height:300px;visibility:hidden;">${remark}</textarea>
		        			<span id="error_remark"></span>
                          </div>
                      </div>
                      <div class="btn-center-box form-actions">
                          <button class="btn btn-primary btn-large mr10" type="button" onclick="onSubmit();"><i class="icon-ok icon-white"></i>保 存</button>
                          <button class="btn btn-large mr10" type="button" onclick="location.href='links.action?mtype=${mtype}'"><i class="icon-repeat"></i>&nbsp;返 回</button>
                      </div>
                  </fieldset>
            </form>
			</div>
		</div>
</body>
</html>