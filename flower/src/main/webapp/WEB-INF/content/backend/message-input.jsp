<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%@ include file="/common/meta.jsp" %>
	<title>短平快网站后台-辅助管理</title>
	<meta content="辅助管理" name="activemenu" />
	<link href="${ctx}/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/> 
	<link href="${ctx}/css/backend.css" type="text/css" rel="stylesheet"/> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
	
</head>
<body>
<div class="wrapper">
		<%@ include file="/common/adminheader.jsp"%>
		<div class="m010">
        <!--导航开始-->
        	<ul class="breadcrumb">
            	<li><a href="${ctx}/backend/index.action"><i class="icon-home ml10"></i>&nbsp;</a></li>
                <li class="divider"></li>
               	<li>辅助管理</li>
               	<li class="divider"></li>
               	<li>留言信息</li>
               	<li class="divider"></li>
               	<li>
               		<s:if test="id!=null">查看</s:if><s:else>新增</s:else>
               	</li>
                <li class="last-divider"></li>
        	</ul>
        <!--导航结束-->
        	<form class="form-horizontal form-group" id="inputForm" name="inputForm" action="message!save.action" method="post">
	   			<input type="hidden" id="id" name="id" value="${id}"/>
                  <fieldset>
                      <div class="control-group">
                          <label class="control-label">标题<b>*</b></label>
                          <div class="controls">
                              ${title}
                          </div>
                      </div>
                      <div class="control-group">
                          <label for="input01" class="control-label" style="vertical-align: top;">内容<b>*</b></label>
                          <div class="controls">
                              ${remark}
                          </div>
                      </div>
                      <div class="btn-center-box form-actions">
                          <!--<button class="btn btn-primary btn-large mr10" type="button" onclick="onSubmit();"><i class="icon-ok icon-white"></i>保 存</button>  -->
                          <button class="btn btn-large mr10" type="button" onclick="location.href='message.action'"><i class="icon-repeat"></i>&nbsp;返 回</button>
                      </div>
                  </fieldset>
            </form>
			</div>
		</div>
</body>
</html>