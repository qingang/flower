<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<div class="index_news_left">
	<p class="course_category_title_01">
		<a href="news-${mtype}.html" class="pull_right"><img src="image/index_btn_more.png" /></a>技术支持
	</p>
	<ul>
		<s:iterator value="techList">
			<li>
				<span class="rr"><s:date name="publishdate" format="yyyy.MM.dd"/></span>
				<s:if test="link!=null && link!=''">
              		<a href="${link}" target="_blank" class="ellipsis" title="${title}">
                      <common:cut len="30" string="${title}"/> 
                   </a>
              	</s:if>
              	<s:else>
                   <a href="news-${type}-${id}.html" title="${title}" class="ellipsis" target="_blank">
                   	<common:cut len="30" string="${title}"/> 
                   </a>
               </s:else>
				
			</li>
		</s:iterator>
	</ul> 
	<div class="index_news_right">
		<img src="${ctx}/image/p1.jpg"  width="290" height="157"/>
	</div>
</div>