<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@ page import="org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter" %>
<%@ page import="org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter" %>
<%@ page import="org.springframework.security.core.AuthenticationException" %>
<%@page import="org.springside.modules.security.springsecurity.SpringSecurityUtils"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>登录短平汽车玻璃网站后台</title>
<%@ include file="/common/meta.jsp" %>
<link href="${ctx}/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet">
<link href="${ctx}/css/backend.css" rel="stylesheet" type="text/css" /> 
<link href="${ctx}/js/validate/jquery.validate.css" type="text/css" rel="stylesheet" />
<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
<script src="${ctx}/js/jquery.cookie.js" type="text/javascript"></script>
<script src="${ctx}/js/validate/jquery.validate.js" type="text/javascript"></script>
<script src="${ctx}/js/validate/messages_cn.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	//聚焦第一个输入框
	$("#j_username").focus();
	initLogin();//记住用户名
	$("#inputForm").validate({
		rules: {
			j_username: "required", 
			j_password: "required" 
		},
		messages: {
			j_username: {
				required: ""
			},
			j_password: {
				required: ""
			} 
		},
        errorPlacement: function(error, element) {   
	        if (document.getElementById("error_"+element.attr("name")))  {
	            error.appendTo("#error_"+element.attr("name"));  
	        }
	        else       
	            error.insertAfter(element);   
	        },
	        errorElement: "em"
	}); 
});
function onSubmit() {
	 if($("#inputForm").valid()){
		 doLogin();
	 	$("#inputForm").submit();
	 }	
}
document.onkeydown = function(e){    
	e = e || window.event;   
	if(e.keyCode === 13){        
	 onSubmit();   
	}
};
function doLogin()  
{  
	/** 实现记住密码功能 */    
    var n = $('#j_username').val();
    if($('#remeberme').attr('checked')){  
        $.cookie('username', n, {expires:7});  
    }else{  
        $.cookie('username', null);  
    }     
}
function initLogin(){
	var n = $.cookie('username');
    if(n!=null) {
        $('#j_username').val(n);
        $('#remeberme').attr('checked',"true");
    }
}
</script>
</head>
<body class="wrapper_bk_box">
	<div class="wrap-container" >
    	<div class="login_logo_box">
        	<p><img src="${ctx}/image/r.png" width="100" height="100"><span class="big_bold_font">短平快汽车玻璃网站后台</span></p>
        	<div class="login_bk_box">
        		<p class="login_title"></p>
            	<form class="form-horizontal" method="post" action="${ctx}/j_spring_security_check" id="inputForm">
                	<div class="control-group">
                        <label class="control-label">用户名</label>
                        <div class="controls" >
                        	<input type="text" name="j_username" id="j_username" class="input-medium" placeholder="请输入用户名"/>
                    	</div>
                	</div>
                	<div class="control-group">
                        <label class="control-label">密码</label>
                    	<div class="controls">
                        	<input type="password" name="j_password" id="j_password" class="input-medium" placeholder="请输入密码"/>
                        	<p class="help-block colorred">
                        	<%
								if (session.getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY) != null) {
						    	if(session.getAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY).toString().contains("LockedException")){
								%>
								登录失败,用户被锁定.
								<%
							}else{%>
								登录失败,用户名或密码错误.
							<%}
								session.removeAttribute(AbstractAuthenticationProcessingFilter.SPRING_SECURITY_LAST_EXCEPTION_KEY);
								}
							%> 
                        </p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                    <input type="checkbox" name="remeberme" id="remeberme" onclick="doLogin();"/><span class="ml5">记住密码</span><a href="#" onclick="onSubmit();" class=" btn btn-primary btn-large ml30"><i class=" icon-ok icon-white"></i>&nbsp;&nbsp;登&nbsp;录</a>
                    </div>
                </div>
            </form>
        
        </div>
	</div>
</div>

</body>
</html>

