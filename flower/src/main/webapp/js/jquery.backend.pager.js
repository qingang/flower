(function($) {
    $.fn.pager = function(options) {
        var opts = $.extend({},
        $.fn.pager.defaults, options);
        return this.each(function() {
            $(this).empty().append(renderpager(parseInt(options.pageTotalCount),parseInt(options.pagenumber), parseInt(options.pagecount), options.buttonClickCallback));
            $('.pages a').mouseover(function() {
                document.body.style.cursor = "pointer"
            }).mouseout(function() {
                document.body.style.cursor = "auto"
            })
        })
    };
    function renderpager(pageTotalCount,pagenumber, pagecount, buttonClickCallback) {
        var totalpage = $("<span class='pull-right' >共" + pagecount + "页;总共"+pageTotalCount+"条记录; </span>");
        var $result=$('<div class="datatables-paginate"><div>')
        var $pdiv = $('<div class="pagination"></div>');
        var $pager = $('<ul></ul>');
        $pager.append(renderButton('首页', pagenumber, pagecount, buttonClickCallback)).append(renderButton('上一页', pagenumber, pagecount, buttonClickCallback));
        var startPoint = 1;
        var endPoint = 9;
        if (pagenumber > 4) {
            startPoint = pagenumber - 4;
            endPoint = pagenumber + 4
        }
        if (endPoint > pagecount) {
            startPoint = pagecount - 8;
            endPoint = pagecount
        }
        if (startPoint < 1) {
            startPoint = 1
        }
        for (var page = startPoint; page <= endPoint; page++) {
            var currentButton = $("<li><a style='cursor: pointer;'>" + (page) + "</a></li>");
            page == pagenumber ? currentButton.addClass('active') : currentButton.click(function() {
                buttonClickCallback(this.firstChild.firstChild.data)
            });
            currentButton.appendTo($pager)
        }
        $pager.append(renderButton('下一页', pagenumber, pagecount, buttonClickCallback)).append(renderButton('尾页', pagenumber, pagecount, buttonClickCallback));
        $pdiv.append($pager);
        $result.append(totalpage);
        $result.append($pdiv);
        return $result;
    }
    function renderButton(buttonLabel, pagenumber, pagecount, buttonClickCallback) {
        var $Button = $("<li><a style='cursor: pointer;'>" + buttonLabel + "</a></li>");
        var destPage = 1;
        switch (buttonLabel) {
        case "首页":
            destPage = 1;
            break;
        case "上一页":
            destPage = pagenumber - 1;
            break;
        case "下一页":
            destPage = pagenumber + 1;
            break;
        case "尾页":
            destPage = pagecount;
            break
        }
        if (buttonLabel == "首页" || buttonLabel == "上一页") {
            pagenumber <= 1 ? $Button.addClass('disabled') : $Button.click(function() {
                buttonClickCallback(destPage)
            })
        } else {
            pagenumber >= pagecount ? $Button.addClass('disabled') : $Button.click(function() {
                buttonClickCallback(destPage)
            })
        }
        return $Button
    }
    $.fn.pager.defaults = {
    	pageTotalCount:1,
        pagenumber: 1,
        pagecount: 1
    }
})(jQuery);