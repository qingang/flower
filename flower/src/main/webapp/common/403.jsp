<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>  
	<title>短平快-汽车服务-汽车玻璃修补-花卉预定</title>
	<link href="${ctx}/css/front.css" rel="stylesheet" type="text/css" /> 
	<script src="${ctx}/js/jquery.js" type="text/javascript"></script>
</head>
<body>
	<div id="wrapper">	
		<%@ include file="/common/header.jsp" %>
		<div id="content">
			<div class="error-pages">
				<div class="con">
        			<p><img src="${ctx}/image/find-bird.png" /></p>
        			<p class="info">您没有访问的此页面的权限！</p>
       
    			</div>
    		</div>
		</div>
		<%@ include file="/common/footer.jsp" %>
	</div>
	<script type="text/javascript" src="js/common.js"></script>
</body>
</html>
