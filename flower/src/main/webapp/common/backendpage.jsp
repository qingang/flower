<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp"%>
<script src="${ctx}/js/jquery.backend.pager.js" type="text/javascript"></script>
<script type="text/javascript">
     $(document).ready(function() {
    	  PageClick = function(pageclickednumber) {
              $("#pager").pager({pageTotalCount:${page.totalCount}, pagenumber: pageclickednumber, pagecount:${page.totalPages}, buttonClickCallback: PageClick });
              jumpPage(pageclickednumber);
          }
          $("#pager").pager({pageTotalCount:${page.totalCount}, pagenumber:${page.pageNo}, pagecount:${page.totalPages}, buttonClickCallback:PageClick });
          $("#mainForm").find("button[type='submit'],input[type='submit']").click(function(){
              $('#pageNo').val(1);
          });
 


        	            

        });
     function jumpPage(pageNo) {
    		$("#pageNo").val(pageNo);
    		$("#mainForm").submit();
     }
     function gotoSubmit(obj,pagecount,callBack){
         var pageGo = document.getElementById('pageGo').value;
         if(pageGo==""){
             alert("页码不能为空");
             document.getElementById('pageGo').focus(); 
             return false;
         }else{
        	 var r = /^\+?[1-9][0-9]*$/;　　//正整数 
             if(!r.test(pageGo) || pageGo > pagecount){
            	 alert("页码不合法");
            	 document.getElementById('pageGo').value='';
                 document.getElementById('pageGo').focus();
                 return false;
             }
         }
         jumpPage(pageGo);
         
     }
</script>
<div id="pager"></div>
