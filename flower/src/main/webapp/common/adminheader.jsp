<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/common/taglibs.jsp" %>
<%@page import="org.springside.modules.security.springsecurity.SpringSecurityUtils"%>
 <script type="text/javascript">
           $(document).ready(function(){
               $("div.tit").click(function(){
                   var isShow = $("div.bg").css("display");
                   if (isShow != "none") {
                       //$("div.bg").show();
                       $("div.bg").slideUp(800);
                   }
                   else {
                       //$("div.bg").show();
                       $("div.bg").slideDown(800);
                   }
               });
           })
</script>
<div class="desktop">
	<div class="bg" style="display:none;">
    	<div class="course-list">
        	<div class="box">
                <h2>资讯管理</h2>
                <ul>
                    <li><a href="news.action?mtype=0"><img src="${ctx}/image/nav_0402.png" />新闻中心</a></li>
                    <li><a href="news.action?mtype=1"><img src="${ctx}/image/nav_0402.png" />技术支持</a></li>
                </ul>
            </div>
        </div>
        <div class="course-list">
        	<div class="box">
                <h2>辅助管理</h2>
                <ul>
                	<li><a href="message.action"><img src="${ctx}/image/nav_0402.png" />留言信息</a></li>
                    <li><a href="aboutus.action"><img src="${ctx}/image/nav_0402.png" />关于我们</a></li>
                </ul>
            </div>
        </div>
        <div class="course-list">
        	<div class="box">
                <h2>花卉管理</h2>
                <ul>
                    <li><a href="flower.action"><img src="${ctx}/image/nav_0402.png" />花卉产品</a></li>
                    <li><a href="order.action"><img src="${ctx}/image/nav_0402.png" />订单中心</a></li>
                </ul>
            </div>
        </div>
        <div class="course-list">
        	<div class="box">
                <h2>链接管理</h2>
                <ul>
                    <li><a href="friendlink.action?mtype=1"><img src="${ctx}/image/nav_0402.png" />友情链接</a></li>
                    <li><a href="links.action?mtype=2"><img src="${ctx}/image/nav_0402.png" />汽车名企</a></li>
                    <li><a href="links.action?mtype=3"><img src="${ctx}/image/nav_0402.png" />汽车玻璃名企</a></li>
                </ul>
            </div>
        </div>

    </div>
	<div class="header">
		<div class="tit">
            <h2 class="ellipsis">
            	<span>欢迎进入网站后台！</span>&nbsp;&nbsp;
            </h2>
        </div>
       <div class="navbar"> 
        	<a  href="user!view.action" title="修改密码"><i class="icon-management nav-icon-white"></i></a>
        
        	<a onclick="if (confirm('确定要退出吗？')) return true; else return false;" title="退出" href="${ctx}/j_spring_security_logout"><i class="icon-logout"></i></a>
     </div>
    </div>
</div>
<div class="makeablank"></div>