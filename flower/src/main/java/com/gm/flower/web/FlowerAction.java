//-------------------------------------------------------------------------
// Copyright (c) 2000-2010 Digital. All Rights Reserved.
//
// This software is the confidential and proprietary information of
// Digital
//
// Original author: qingang
//
//-------------------------------------------------------------------------
// LOOSOFT MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. UFINITY SHALL NOT BE
// LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
// MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
//
// THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
// CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
// PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
// NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
// SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
// SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
// PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). UFINITY
// SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
// HIGH RISK ACTIVITIES.
//-------------------------------------------------------------------------
package com.gm.flower.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.orm.PropertyFilter;
import org.springside.modules.utils.web.struts2.Struts2Utils;

import cn.common.lib.springside.util.ParamPropertyUtils;
import cn.common.lib.springside.web.CrudActionSupport;

import com.gm.flower.contant.Global;
import com.gm.flower.core.FlowerManager;
import com.gm.flower.entity.Flower;
import com.google.common.collect.Lists;

/**
 * 
 * 花卉产品action
 * 
 * @author qingang
 * @version 1.0
 * @since 2012-7-28
 */
@Namespace("/")
@Results({ @Result(name = CrudActionSupport.RELOAD, location = "flower.action", type = "redirect") })
public class FlowerAction extends CrudActionSupport<Flower> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Flower entity;

	private List<Long> ids;

	private int num;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	private Page<Flower> page = new Page<Flower>(10);

	private List<Flower> flowerList = Lists.newArrayList();

	public List<Flower> getFlowerList() {
		return flowerList;
	}

	@Autowired
	private FlowerManager flowerManager;

	@Override
	public String input() throws Exception {
		return INPUT;
	}

	public String index() throws Exception {
		flowerList = flowerManager.search(num);
		return "index";
	}

	@Override
	public String delete() throws Exception {
		try {
			flowerManager.delete(id);
			this.addActionMessage(Global.DELETE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			this.addActionMessage(Global.DELETE_LOSE);
		}
		return RELOAD;
	}

	@Override
	public String list() throws Exception {
		try {
			HttpServletRequest request = Struts2Utils.getRequest();
			List<PropertyFilter> filters = PropertyFilter
					.buildFromHttpRequest(request);
			ParamPropertyUtils.replacePropertyRule(filters);
			filters.add(new PropertyFilter("EQB_ispublic", "true"));
			if (!page.isOrderBySetted()) {
				page.setOrderBy("id");
				page.setOrder(Page.DESC);
			}
			page = flowerManager.search(page, filters);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = flowerManager.get(id);
		} else {
			entity = new Flower();
		}

	}

	@Override
	public String save() throws Exception {

		return RELOAD;
	}

	@Override
	public Flower getModel() {
		return entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Flower getEntity() {
		return entity;
	}

	public void setEntity(Flower entity) {
		this.entity = entity;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Page<Flower> getPage() {
		return page;
	}

	public void setPage(Page<Flower> page) {
		this.page = page;
	}

}
