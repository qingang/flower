//-------------------------------------------------------------------------
// Copyright (c) 2000-2010 Digital. All Rights Reserved.
//
// This software is the confidential and proprietary information of
// Digital
//
// Original author: qingang
//
//-------------------------------------------------------------------------
// LOOSOFT MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. UFINITY SHALL NOT BE
// LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
// MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
//
// THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
// CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
// PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
// NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
// SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
// SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
// PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). UFINITY
// SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
// HIGH RISK ACTIVITIES.
//-------------------------------------------------------------------------
package com.gm.flower.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.orm.PropertyFilter;
import org.springside.modules.utils.web.struts2.Struts2Utils;

import cn.common.lib.springside.util.ParamPropertyUtils;
import cn.common.lib.springside.web.CrudActionSupport;
import cn.common.lib.vo.LabelValue;

import com.gm.flower.contant.Global;
import com.gm.flower.core.LinksManager;
import com.gm.flower.entity.Links;
import com.google.common.collect.Lists;

/**
 * 
 * 友情链接、广告推广action
 * 
 * @author qingang
 * @version 1.0
 * @since 2012-7-28
 */
@Namespace("/")
@Results({ @Result(name = CrudActionSupport.RELOAD, location = "friendlink.action?mtype=${mtype}", type = "redirect") })
public class LinksAction extends CrudActionSupport<Links> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Links entity;

	private List<Long> ids;

	private Page<Links> page = new Page<Links>(28);

	List<Links> linksList = Lists.newArrayList();

	public List<Links> getLinksList() {
		return linksList;
	}

	private int ltype;

	private int num;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getMtype() {
		return ltype;
	}

	public void setLtype(int ltype) {
		this.ltype = ltype;
	}

	public LabelValue getTypeName() {
		return Global.linksTypeList.get(ltype);
	}

	@Autowired
	private LinksManager linksManager;

	@Override
	public String input() throws Exception {
		return INPUT;
	}

	/**
	 * 
	 * 友情链接 文字
	 * 
	 * @since 2013-7-23
	 * @author qingang
	 * @return
	 * @throws Exception
	 */
	public String friend() throws Exception {
		linksList = linksManager.search(ltype, num);
		return "friend";
	}

	/**
	 * 
	 * 链接图片
	 * 
	 * @since 2013-7-23
	 * @author qingang
	 * @return
	 * @throws Exception
	 */
	public String left() throws Exception {
		linksList = linksManager.search(ltype, num);
		return "left";
	}

	@Override
	public String list() throws Exception {
		try {
			HttpServletRequest request = Struts2Utils.getRequest();
			List<PropertyFilter> filters = PropertyFilter
					.buildFromHttpRequest(request);
			ParamPropertyUtils.replacePropertyRule(filters);
			filters.add(new PropertyFilter("EQI_type", "" + ltype));
			filters.add(new PropertyFilter("EQB_ispublic", "true"));
			if (!page.isOrderBySetted()) {
				page.setOrderBy("id");
				page.setOrder(Page.DESC);
			}
			page = linksManager.search(page, filters);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	/**
	 * 
	 * 关于我们
	 * 
	 * @since 2013-7-24
	 * @author qingang
	 * @return
	 * @throws Exception
	 */
	public String aboutus() throws Exception {
		entity = linksManager.getAboutus();
		return "aboutus";
	}

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = linksManager.get(id);
		} else {
			entity = new Links();
		}

	}

	@Override
	public String save() throws Exception {

		return RELOAD;
	}

	@Override
	public Links getModel() {
		return entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Links getEntity() {
		return entity;
	}

	public void setEntity(Links entity) {
		this.entity = entity;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Page<Links> getPage() {
		return page;
	}

	public void setPage(Page<Links> page) {
		this.page = page;
	}

}
