//-------------------------------------------------------------------------
// Copyright (c) 2000-2010 Digital. All Rights Reserved.
//
// This software is the confidential and proprietary information of
// Digital
//
// Original author: qingang
//
//-------------------------------------------------------------------------
// LOOSOFT MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. UFINITY SHALL NOT BE
// LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
// MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
//
// THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
// CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
// PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
// NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
// SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
// SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
// PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). UFINITY
// SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
// HIGH RISK ACTIVITIES.
//-------------------------------------------------------------------------
package com.gm.flower.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.orm.PropertyFilter;
import org.springside.modules.utils.web.struts2.Struts2Utils;

import cn.common.lib.springside.util.ParamPropertyUtils;
import cn.common.lib.springside.web.CrudActionSupport;
import cn.common.lib.vo.LabelValue;

import com.gm.flower.contant.Global;
import com.gm.flower.core.NewsManager;
import com.gm.flower.entity.News;
import com.google.common.collect.Lists;

/**
 * 
 * 新闻资讯前台action
 * 
 * @author qingang
 * @version 1.0
 * @since 2012-7-28
 */
@Namespace("/")
@Results({ @Result(name = CrudActionSupport.RELOAD, location = "news.action", type = "redirect") })
public class NewsAction extends CrudActionSupport<News> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private News entity;

	private List<Long> ids = Lists.newArrayList();

	private Page<News> page = new Page<News>(20);

	private List<News> newsList = Lists.newArrayList();

	private List<News> techList = Lists.newArrayList();

	public List<News> getTechList() {
		return techList;
	}

	private int num;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public List<News> getNewsList() {
		return newsList;
	}

	private int mtype; // 0:新闻焦点

	// 1：热点资讯

	public int getMtype() {
		return mtype;
	}

	public void setMtype(int mtype) {
		this.mtype = mtype;
	}

	/**
	 * 行情资讯类别名称
	 */
	public LabelValue getTypeName() {
		return Global.newsTypeList.get(mtype);
	}

	@Autowired
	private NewsManager newsManager;

	@Override
	public String input() throws Exception {
		return INPUT;
	}

	/**
	 * 
	 * 技术支持
	 * 
	 * @since 2013-7-23
	 * @author qingang
	 * @return
	 * @throws Exception
	 */
	public String technology() throws Exception {
		techList = newsManager.search(mtype, num);
		return "technology";
	}

	/**
	 * 
	 * 新闻资讯
	 * 
	 * @since 2013-7-23
	 * @author qingang
	 * @return
	 * @throws Exception
	 */
	public String center() throws Exception {
		newsList = newsManager.search(mtype, num);
		return "center";
	}

	@Override
	public String list() throws Exception {
		try {
			HttpServletRequest request = Struts2Utils.getRequest();
			List<PropertyFilter> filters = PropertyFilter
					.buildFromHttpRequest(request);
			ParamPropertyUtils.replacePropertyRule(filters);
			filters.add(new PropertyFilter("EQI_type", "" + mtype));
			filters.add(new PropertyFilter("EQB_ispublish", "true"));
			if (!page.isOrderBySetted()) {
				page.setOrderBy("id");
				page.setOrder(Page.DESC);
			}
			page = newsManager.search(page, filters);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return SUCCESS;
	}

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = newsManager.get(id);
		} else {
			entity = new News();
		}

	}

	@Override
	public String save() throws Exception {

		return RELOAD;
	}

	@Override
	public News getModel() {
		return entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public News getEntity() {
		return entity;
	}

	public void setEntity(News entity) {
		this.entity = entity;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Page<News> getPage() {
		return page;
	}

	public void setPage(Page<News> page) {
		this.page = page;
	}

}
