//-------------------------------------------------------------------------
// Copyright (c) 2000-2010 Digital. All Rights Reserved.
//
// This software is the confidential and proprietary information of
// Digital
//
// Original author: qingang
//
//-------------------------------------------------------------------------
// LOOSOFT MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. UFINITY SHALL NOT BE
// LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
// MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
//
// THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
// CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
// PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
// NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
// SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
// SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
// PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). UFINITY
// SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
// HIGH RISK ACTIVITIES.
//-------------------------------------------------------------------------
package com.gm.flower.web;

import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;
import org.springside.modules.orm.Page;
import org.springside.modules.utils.web.struts2.Struts2Utils;

import cn.common.lib.springside.web.CrudActionSupport;

import com.gm.flower.core.MessageManager;
import com.gm.flower.entity.Message;

/**
 * 
 * 留言公告action
 * 
 * @author qingang
 * @version 1.0
 * @since 2012-7-28
 */
@Namespace("/")
@Results({ @Result(name = CrudActionSupport.RELOAD, location = "message!input.action", type = "redirect") })
public class MessageAction extends CrudActionSupport<Message> {

	/**
	 * serialVersionUID long
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private Message entity;

	private List<Long> ids;

	private Page<Message> page = new Page<Message>(10);

	@Autowired
	private MessageManager messageManager;

	@Override
	public String input() throws Exception {
		return SUCCESS;
	}

	@Override
	public String list() throws Exception {

		return SUCCESS;
	}

	@Override
	protected void prepareModel() throws Exception {
		if (id != null) {
			entity = messageManager.get(id);
		} else {
			entity = new Message();
		}

	}

	@Override
	public String save() throws Exception {
		String scode = (String) Struts2Utils.getSessionAttribute("scode");
		String code = Struts2Utils.getParameter("code");
		if (code == null || !code.equalsIgnoreCase(scode)) {
			this.addActionMessage("验证码输入不正确!");
		} else {
			try {
				if (id == null) {
					entity.setCreatedate(new Date());
					entity.setIspublic(false);
				}
				messageManager.save(entity);
				this.addActionMessage("留言成功");
			} catch (Exception e) {
				this.addActionMessage("留言失败");
				e.printStackTrace();
			}
		}
		return RELOAD;
	}

	@Override
	public Message getModel() {
		return entity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Message getEntity() {
		return entity;
	}

	public void setEntity(Message entity) {
		this.entity = entity;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}

	public Page<Message> getPage() {
		return page;
	}

	public void setPage(Page<Message> page) {
		this.page = page;
	}

}
