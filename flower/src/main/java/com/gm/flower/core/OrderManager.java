//-------------------------------------------------------------------------
// Copyright (c) 2000-2010 Digital. All Rights Reserved.
//
// This software is the confidential and proprietary information of
// Digital
//
// Original author: qingang
//
//-------------------------------------------------------------------------
// LOOSOFT MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
// TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
// PARTICULAR PURPOSE, OR NON-INFRINGEMENT. UFINITY SHALL NOT BE
// LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
// MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
//
// THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
// CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
// PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
// NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
// SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
// SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
// PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). UFINITY
// SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
// HIGH RISK ACTIVITIES.
//-------------------------------------------------------------------------
package com.gm.flower.core;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import cn.common.lib.springside.orm.BaseDao;
import cn.common.lib.springside.service.EntityManager;

import com.gm.flower.api.OrderDao;
import com.gm.flower.entity.Order;

/**
 * 花卉订单业务逻辑类
 * 
 * @author qingang
 * @version 1.0
 * @since 2012-7-25
 */
@Component
@Transactional
public class OrderManager extends EntityManager<Order, Long> {
	@Autowired
	private OrderDao orderDao;

	/**
	 * {@inheritDoc}
	 * 
	 * @since 2012-7-25
	 * @see cn.common.lib.springside.service.EntityManager#getEntityDao()
	 */
	@Override
	protected BaseDao<Order, Long> getEntityDao() {
		return orderDao;
	}

	/**
	 * 
	 * 批量删除
	 * 
	 * @since 2012-7-26
	 * @author qingang
	 * @param ids
	 */
	public void batchDelete(List<Long> ids) {
		orderDao.batchDelete(ids);
	}

	/**
	 * 
	 * 留言公告信息标记已读
	 * 
	 * @since 2012-7-26
	 * @author qingang
	 * @param ids
	 */
	public void publish(List<Long> ids) {
		orderDao.publish(ids, true);

	}

	/**
	 * 
	 * 留言公告信息标记未读
	 * 
	 * @since 2012-7-26
	 * @author qingang
	 * @param ids
	 */
	public void cancelPublish(List<Long> ids) {
		orderDao.publish(ids, false);
	}

}
